//! Mint object format
use crate::common::errors::{MintError, Result};
use crate::common::games::MintVersion;
use crate::common::xbin::XBIN;
use byteorder::{BigEndian, ReadBytesExt};
use std::collections::HashSet;
use std::io::{Read, Seek, SeekFrom};

/// Raw Mint file header
#[derive(Clone, Eq, PartialEq, Debug)]
struct RawMintHeader {
    version: (u8, u8, u8, u8),
    mint_name_off: u32,
    sdata_off: u32,
    symbol_table: u32,
    objects: u32,
    unk: u32,
}

impl RawMintHeader {
    pub fn read<R>(reader: &mut R) -> Result<RawMintHeader>
    where
        R: Read,
    {
        let mut version = [0u8; 4];
        reader.read_exact(&mut version)?;
        Ok(RawMintHeader {
            version: (version[0], version[1], version[2], version[3]),
            mint_name_off: reader.read_u32::<BigEndian>()?,
            sdata_off: reader.read_u32::<BigEndian>()?,
            symbol_table: reader.read_u32::<BigEndian>()?,
            objects: reader.read_u32::<BigEndian>()?,
            unk: reader.read_u32::<BigEndian>()?,
        })
    }
}

/// Mint file header
#[derive(Clone, Eq, PartialEq, Debug)]
pub struct MintHeader {
    pub header: XBIN,
    pub version: MintVersion,
    pub name: String,
    pub sdata: Vec<u8>,
    pub symbol_list: Vec<String>,
    pub objects: Vec<Object>,
}

impl MintHeader {
    pub fn read<R>(reader: &mut R) -> Result<MintHeader>
    where
        R: Read + Seek,
    {
        let header = XBIN::load(reader)?;
        let raw_header = RawMintHeader::read(reader)?;
        let version = match raw_header.version {
            (0, 2, 0, 0) => Ok(MintVersion::V0_2_0),
            _ => Err(MintError::InvalidHeader),
        }?;
        Ok(MintHeader {
            header,
            version,
            name: {
                reader.seek(SeekFrom::Start(u64::from(raw_header.mint_name_off)))?;
                read_string(reader)?
            },
            sdata: {
                reader.seek(SeekFrom::Start(u64::from(raw_header.sdata_off)))?;
                let mut buf = (0..(raw_header.symbol_table - raw_header.sdata_off))
                    .map(|_| 0u8)
                    .collect::<Vec<_>>();
                reader.read_exact(&mut buf)?;
                buf
            },
            symbol_list: {
                reader.seek(SeekFrom::Start(u64::from(raw_header.symbol_table)))?;
                let x: Result<Vec<_>> = read_list(reader)?
                    .iter()
                    .map(|off| {
                        reader.seek(SeekFrom::Start(u64::from(*off)))?;
                        read_string(reader)
                    })
                    .collect();
                x?
            },
            objects: {
                reader.seek(SeekFrom::Start(u64::from(raw_header.objects)))?;
                let x: Result<Vec<_>> = read_list(reader)?
                    .iter()
                    .map(|off| {
                        reader.seek(SeekFrom::Start(u64::from(*off)))?;
                        Object::read(reader)
                    })
                    .collect();
                x?
            },
        })
    }
}

/// Raw Object Header
#[derive(Clone, PartialEq, Eq, Debug)]
struct RawObjectHeader {
    name_off: u32,
    field_off: u32,
    code_off: u32,
}

impl RawObjectHeader {
    pub fn read<R>(reader: &mut R) -> Result<RawObjectHeader>
    where
        R: Read,
    {
        Ok(RawObjectHeader {
            name_off: reader.read_u32::<BigEndian>()?,
            field_off: reader.read_u32::<BigEndian>()?,
            code_off: reader.read_u32::<BigEndian>()?,
        })
    }
}

/// Object declaration
#[derive(Clone, PartialEq, Eq, Debug)]
pub struct Object {
    pub name: String,
    pub fields: Vec<MemberField>,
    pub methods: Vec<Method>,
}

impl Object {
    pub fn read<R>(reader: &mut R) -> Result<Object>
    where
        R: Read + Seek,
    {
        let header = RawObjectHeader::read(reader)?;
        Ok(Object {
            name: {
                reader.seek(SeekFrom::Start(u64::from(header.name_off)))?;
                read_string(reader)?
            },
            fields: {
                reader.seek(SeekFrom::Start(u64::from(header.field_off)))?;
                let x: Result<Vec<_>> = read_list(reader)?
                    .iter()
                    .map(|off| {
                        reader.seek(SeekFrom::Start(u64::from(*off)))?;
                        MemberField::read(reader)
                    })
                    .collect();
                x?
            },
            methods: {
                reader.seek(SeekFrom::Start(u64::from(header.code_off)))?;
                let x: Result<Vec<_>> = read_list(reader)?
                    .iter()
                    .map(|off| {
                        reader.seek(SeekFrom::Start(u64::from(*off)))?;
                        Method::read(reader)
                    })
                    .collect();
                x?
            },
        })
    }
}

/// Raw Field Membe Header
#[derive(Clone, PartialEq, Eq, Debug)]
struct RawMemberFieldHeader {
    name_off: u32,
    type_off: u32,
}

impl RawMemberFieldHeader {
    pub fn read<R>(reader: &mut R) -> Result<RawMemberFieldHeader>
    where
        R: Read,
    {
        Ok(RawMemberFieldHeader {
            name_off: reader.read_u32::<BigEndian>()?,
            type_off: reader.read_u32::<BigEndian>()?,
        })
    }
}

/// Member field definition
#[derive(Clone, PartialEq, Eq, Debug)]
pub struct MemberField {
    pub r#type: String,
    pub name: String,
}

impl MemberField {
    pub fn read<R>(reader: &mut R) -> Result<MemberField>
    where
        R: Read + Seek,
    {
        let header = RawMemberFieldHeader::read(reader)?;
        Ok(MemberField {
            r#type: {
                reader.seek(SeekFrom::Start(u64::from(header.type_off)))?;
                read_string(reader)?
            },
            name: {
                reader.seek(SeekFrom::Start(u64::from(header.name_off)))?;
                read_string(reader)?
            },
        })
    }
}

/// Raw Method definition
#[derive(Clone, PartialEq, Eq, Debug)]
struct RawMethodField {
    name_off: u32,
    code_off: u32,
}

impl RawMethodField {
    pub fn read<R>(reader: &mut R) -> Result<RawMethodField>
    where
        R: Read,
    {
        Ok(RawMethodField {
            name_off: reader.read_u32::<BigEndian>()?,
            code_off: reader.read_u32::<BigEndian>()?,
        })
    }
}

/// Method definition
#[derive(Clone, PartialEq, Eq, Debug)]
pub struct Method {
    pub name: String,
    pub code: Vec<u8>,
}

impl Method {
    pub fn read<R>(reader: &mut R) -> Result<Method>
    where
        R: Read + Seek,
    {
        let header = RawMethodField::read(reader)?;
        Ok(Method {
            name: {
                reader.seek(SeekFrom::Start(u64::from(header.name_off)))?;
                read_string(reader)?
            },
            code: {
                reader.seek(SeekFrom::Start(u64::from(header.code_off)))?;
                read_code(reader)?
            },
        })
    }
}

fn crappy_mint_sim<'a, R>(
    reader: &'a mut R,
    hsr: &'a mut HashSet<u64>,
    mut pc: u64,
) -> Result<&'a mut HashSet<u64>>
where
    R: Read + Seek,
{
    loop {
        if hsr.contains(&pc) {
            return Ok(hsr);
        }
        hsr.insert(pc);
        let mut ins = [0u8; 4];
        reader.read_exact(&mut ins)?;
        // "Interpret" the instruction
        assert_ne!(ins[0], 0);
        match ins[0] {
            0x34 | 0x35 => {
                return Ok(hsr); // We are done here
            }
            0x30 => {
                // Unconditional jump
                let raw_off: u16 = (u16::from(ins[2]) << 8) + u16::from(ins[3]);
                let off: i16 = raw_off as i16; // Wrap intentional
                let off: i64 = i64::from(off) * 4;
                reader.seek(SeekFrom::Current(off - 4))?;
                pc = pc.wrapping_add(off as u64);
                continue;
            }
            0x31 | 0x32 => {
                // Conditional jump
                let raw_off: u16 = (u16::from(ins[2]) << 8) + u16::from(ins[3]);
                let off: i16 = raw_off as i16; // Wrap intentional
                let off: i64 = i64::from(off) * 4;
                reader.seek(SeekFrom::Current(off - 4))?;
                // check the branch taken state
                let _ = crappy_mint_sim(reader, hsr, pc.wrapping_add(off as u64))?;
                // continue to emulate the branch not taken state
                reader.seek(SeekFrom::Start(pc))?;
            }
            _ => {}
        }
        pc += 4;
    }
}

/// Reads MINT code from a reader
pub fn read_code<R>(reader: &mut R) -> Result<Vec<u8>>
where
    R: Read + Seek,
{
    let start_off = reader.seek(SeekFrom::Current(0))?;
    let mut hs: HashSet<u64> = HashSet::new();
    let hsr = &mut hs;
    let hsr = crappy_mint_sim(reader, hsr, start_off)?;
    let first = *hsr.iter().min().unwrap();
    let length = (*hsr.iter().max().unwrap() + 4) - first;
    reader.seek(SeekFrom::Start(first))?;
    let mut buf = (0..length).map(|_| 0u8).collect::<Vec<u8>>();
    reader.read_exact(&mut buf)?;
    Ok(buf)
}

/// Reads a string from a reader
pub fn read_string<R>(reader: &mut R) -> Result<String>
where
    R: Read,
{
    let size = reader.read_u32::<BigEndian>()?;
    let mut buf: Vec<_> = (0..size).map(|_| 0u8).collect();
    reader.read_exact(&mut buf)?;
    Ok(String::from_utf8(buf)?)
}

/// Reads a list from a reader
pub fn read_list<R>(reader: &mut R) -> Result<Vec<u32>>
where
    R: Read,
{
    let size = reader.read_u32::<BigEndian>()?;
    let buf: std::result::Result<Vec<_>, _> =
        (0..size).map(|_| reader.read_u32::<BigEndian>()).collect();
    Ok(buf?)
}
