//! Decoder for mint modules
use super::super::mintobj::Method;
use super::constants::{OpFormat, OpType};
use std::fmt;

/// Instruction Object. This object contains all necessary informations for a single instruction
#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub struct Instruction {
    pub opcode: OpType,
    pub data: OpFormat,
}

macro_rules! opcode {
    ($self: expr, $format: pat, $writeexpr: stmt) => {
        if let $format = $self.data {
            $writeexpr
        } else {
            unsafe {
                std::hint::unreachable_unchecked() // should never happen
            }
        }
    };
}

impl fmt::Display for Instruction {
    #[allow(clippy::cyclomatic_complexity)] // Blame it on all of the if expressions the macro adds
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self.opcode {
            OpType::LDSRZR => opcode!(self, OpFormat::Sr1(sr1), write!(f, "ld b{}, false", sr1)),
            OpType::LDSRBT => opcode!(self, OpFormat::Sr1(sr1), write!(f, "ld b{}, true", sr1)),
            OpType::LDSRC4 => opcode!(
                self,
                OpFormat::Sr1Cti(sr1, cti),
                write!(f, "ld r{}, [constant:{}]", sr1, cti)
            ),
            OpType::LDSRCA => opcode!(
                self,
                OpFormat::Sr1Cti(sr1, cti),
                write!(f, "ld r{}, constant:{}", sr1, cti)
            ),
            OpType::LDSRSR => opcode!(
                self,
                OpFormat::Sr1Sr2(sr1, sr2),
                write!(f, "ld r{}, r{}", sr1, sr2)
            ),
            OpType::LDSRFZ => opcode!(self, OpFormat::Sr1(sr1), write!(f, "ld r{}, fr0", sr1)),
            OpType::LDFRSR => opcode!(
                self,
                OpFormat::Sr1Fr(sr1, fr),
                write!(f, "ld fr{}, r{}", fr, sr1)
            ),
            OpType::LDSRSV => opcode!(
                self,
                OpFormat::Sr1Sti(sr1, sti),
                write!(f, "ld r{}, [static:{}]", sr1, sti)
            ),
            OpType::LDSRA4 => opcode!(
                self,
                OpFormat::Sr1Sr2(sr1, sr2),
                write!(f, "ld r{}, [r{}]", sr1, sr2)
            ),
            OpType::LDSRSZ => opcode!(
                self,
                OpFormat::Sr1Sti(sr1, sti),
                write!(f, "ld r{}, sizeof:[static:{}]", sr1, sti)
            ),
            OpType::STSRSR => opcode!(
                self,
                OpFormat::Sr1Sr2(sr1, sr2),
                write!(f, "ld [r{}], r{}", sr1, sr2)
            ),
            OpType::STSVSR => opcode!(
                self,
                OpFormat::Sr1Sti(sr1, sti),
                write!(f, "ld [static:{}], r{}", sti, sr1)
            ),
            OpType::ADDI32 => opcode!(
                self,
                OpFormat::Sr1Sr2Sr3(sr1, sr2, sr3),
                write!(f, "add r{}, r{}, r{}", sr1, sr2, sr3)
            ),
            OpType::SUBI32 => opcode!(
                self,
                OpFormat::Sr1Sr2Sr3(sr1, sr2, sr3),
                write!(f, "sub r{}, r{}, r{}", sr1, sr2, sr3)
            ),
            OpType::MULS32 => opcode!(
                self,
                OpFormat::Sr1Sr2Sr3(sr1, sr2, sr3),
                write!(f, "muls r{}, r{}, r{}", sr1, sr2, sr3)
            ),
            OpType::DIVS32 => opcode!(
                self,
                OpFormat::Sr1Sr2Sr3(sr1, sr2, sr3),
                write!(f, "divs r{}, r{}, r{}", sr1, sr2, sr3)
            ),
            OpType::MODS32 => opcode!(
                self,
                OpFormat::Sr1Sr2Sr3(sr1, sr2, sr3),
                write!(f, "mods r{}, r{}, r{}", sr1, sr2, sr3)
            ),
            OpType::INCI32 => opcode!(self, OpFormat::Sr1(sr1), write!(f, "inc r{}", sr1)),
            OpType::DECI32 => opcode!(self, OpFormat::Sr1(sr1), write!(f, "dec r{}", sr1)),
            OpType::NEGS32 => opcode!(
                self,
                OpFormat::Sr1Sr2(sr1, sr2),
                write!(f, "neg r{}, r{}", sr1, sr2)
            ),
            OpType::ADDF32 => opcode!(
                self,
                OpFormat::Sr1Sr2Sr3(sr1, sr2, sr3),
                write!(f, "add f{}, f{}, f{}", sr1, sr2, sr3)
            ),
            OpType::SUBF32 => opcode!(
                self,
                OpFormat::Sr1Sr2Sr3(sr1, sr2, sr3),
                write!(f, "sub f{}, f{}, f{}", sr1, sr2, sr3)
            ),
            OpType::MULF32 => opcode!(
                self,
                OpFormat::Sr1Sr2Sr3(sr1, sr2, sr3),
                write!(f, "mul f{}, f{}, f{}", sr1, sr2, sr3)
            ),
            OpType::DIVF32 => opcode!(
                self,
                OpFormat::Sr1Sr2Sr3(sr1, sr2, sr3),
                write!(f, "div f{}, f{}, f{}", sr1, sr2, sr3)
            ),
            OpType::INCF32 => opcode!(self, OpFormat::Sr1(sr1), write!(f, "inc f{}", sr1)),
            OpType::DECF32 => opcode!(self, OpFormat::Sr1(sr1), write!(f, "dec f{}", sr1)),
            OpType::NEGF32 => opcode!(
                self,
                OpFormat::Sr1Sr2(sr1, sr2),
                write!(f, "neg f{}, f{}", sr1, sr2)
            ),
            OpType::LTS32 => opcode!(
                self,
                OpFormat::Sr1Sr2Sr3(sr1, sr2, sr3),
                write!(f, "lts b{}, r{}, r{}", sr1, sr2, sr3)
            ),
            OpType::LES32 => opcode!(
                self,
                OpFormat::Sr1Sr2Sr3(sr1, sr2, sr3),
                write!(f, "les b{}, r{}, r{}", sr1, sr2, sr3)
            ),
            OpType::EQI32 => opcode!(
                self,
                OpFormat::Sr1Sr2Sr3(sr1, sr2, sr3),
                write!(f, "eq b{}, r{}, r{}", sr1, sr2, sr3)
            ),
            OpType::NEI32 => opcode!(
                self,
                OpFormat::Sr1Sr2Sr3(sr1, sr2, sr3),
                write!(f, "ne b{}, r{}, r{}", sr1, sr2, sr3)
            ),
            OpType::LTF32 => opcode!(
                self,
                OpFormat::Sr1Sr2Sr3(sr1, sr2, sr3),
                write!(f, "lt b{}, f{}, f{}", sr1, sr2, sr3)
            ),
            OpType::LEF32 => opcode!(
                self,
                OpFormat::Sr1Sr2Sr3(sr1, sr2, sr3),
                write!(f, "le b{}, f{}, f{}", sr1, sr2, sr3)
            ),
            OpType::EQF32 => opcode!(
                self,
                OpFormat::Sr1Sr2Sr3(sr1, sr2, sr3),
                write!(f, "eq b{}, f{}, f{}", sr1, sr2, sr3)
            ),
            OpType::NEF32 => opcode!(
                self,
                OpFormat::Sr1Sr2Sr3(sr1, sr2, sr3),
                write!(f, "ne b{}, f{}, f{}", sr1, sr2, sr3)
            ),
            OpType::LTCMP => opcode!(
                self,
                OpFormat::Sr1Sr2(sr1, sr2),
                write!(f, "ltcmp b{}, r{}", sr1, sr2)
            ),
            OpType::LECMP => opcode!(
                self,
                OpFormat::Sr1Sr2(sr1, sr2),
                write!(f, "ltcmp b{}, r{}", sr1, sr2)
            ),
            OpType::EQBOOL => opcode!(
                self,
                OpFormat::Sr1Sr2Sr3(sr1, sr2, sr3),
                write!(f, "eq b{}, b{}, b{}", sr1, sr2, sr3)
            ),
            OpType::NEBOOL => opcode!(
                self,
                OpFormat::Sr1Sr2Sr3(sr1, sr2, sr3),
                write!(f, "eq b{}, b{}, b{}", sr1, sr2, sr3)
            ),
            OpType::ANDI32 => opcode!(
                self,
                OpFormat::Sr1Sr2Sr3(sr1, sr2, sr3),
                write!(f, "and r{}, r{}, r{}", sr1, sr2, sr3)
            ),
            OpType::ORI32 => opcode!(
                self,
                OpFormat::Sr1Sr2Sr3(sr1, sr2, sr3),
                write!(f, "or r{}, r{}, r{}", sr1, sr2, sr3)
            ),
            OpType::XORI32 => opcode!(
                self,
                OpFormat::Sr1Sr2Sr3(sr1, sr2, sr3),
                write!(f, "xor r{}, r{}, r{}", sr1, sr2, sr3)
            ),
            OpType::NTI32 => opcode!(
                self,
                OpFormat::Sr1Sr2(sr1, sr2),
                write!(f, "not r{}, r{}", sr1, sr2)
            ),
            OpType::NTBOOL => opcode!(
                self,
                OpFormat::Sr1Sr2(sr1, sr2),
                write!(f, "not b{}, b{}", sr1, sr2)
            ),
            OpType::SLLI32 => opcode!(
                self,
                OpFormat::Sr1Sr2Sr3(sr1, sr2, sr3),
                write!(f, "lsl r{}, r{}, r{}", sr1, sr2, sr3)
            ),
            OpType::SLRI32 => opcode!(
                self,
                OpFormat::Sr1Sr2Sr3(sr1, sr2, sr3),
                write!(f, "lsr r{}, r{}, r{}", sr1, sr2, sr3)
            ),
            OpType::JMP => opcode!(self, OpFormat::Relative(r), write!(f, "jmp {}", r)),
            OpType::JMPPOS => opcode!(
                self,
                OpFormat::Sr1Cs2(sr1, r),
                write!(f, "jmppos r{}, {}", sr1, r)
            ),
            OpType::JMPNEG => opcode!(
                self,
                OpFormat::Sr1Cs2(sr1, r),
                write!(f, "jmpneg r{}, {}", sr1, r)
            ),
            OpType::FENTER => opcode!(
                self,
                OpFormat::ArgsRegs(sr1, sr2),
                write!(f, "fenter {}, {}", sr1, sr2)
            ),
            OpType::FLEAVE => opcode!(self, OpFormat::Implied, write!(f, "fleave")),
            OpType::FRET => opcode!(self, OpFormat::Implied, write!(f, "fret r0")),
            OpType::CALL => opcode!(self, OpFormat::Fun(fun), write!(f, "call function:{}", fun)),
            OpType::YIELD => opcode!(self, OpFormat::Sr1(sr1), write!(f, "yield r{}", sr1)),
            OpType::MCOPY => opcode!(
                self,
                OpFormat::Sr1Sr2Sr3(sr1, sr2, sr3),
                write!(f, "mcopy [r{}], [r{}], r{}", sr1, sr2, sr3)
            ),
            OpType::MZEROS => opcode!(
                self,
                OpFormat::Sr1Sti(sr1, sti),
                write!(f, "mzeros [r{}], sizeof:[static:{}]", sr1, sti)
            ),
            OpType::SPPSH => opcode!(
                self,
                OpFormat::Sr1Sti(sr1, sti),
                write!(f, "new r{}, static:{}", sr1, sti)
            ),
            OpType::SPPSHZ => opcode!(
                self,
                OpFormat::Sr1Sti(sr1, sti),
                write!(f, "newz r{}, static:{}", sr1, sti)
            ),
            OpType::SPPOP => opcode!(
                self,
                OpFormat::Sr1Sti(sr1, sti),
                write!(f, "delete [r{}], static:{}", sr1, sti)
            ),
            OpType::ADDOFS => opcode!(
                self,
                OpFormat::Sr1Sti(sr1, sti),
                write!(f, "add r{}, r{}, offset:{}", sr1, sr1, sti)
            ),
            OpType::ARPSHZ => opcode!(
                self,
                OpFormat::Sr1(sr1),
                write!(f, "new[] r{}, r{}", sr1, sr1)
            ),
            OpType::ARIDX => opcode!(
                self,
                OpFormat::Sr1Sr2(sr1, sr2),
                write!(f, "add r{}, r{}, offset:r{}", sr1, sr1, sr2)
            ),
            OpType::ARLEN => opcode!(
                self,
                OpFormat::Sr1Sr2(sr1, sr2),
                write!(f, "ld r{}, sizeof:r{}", sr1, sr2)
            ),
            OpType::ARPOP => opcode!(self, OpFormat::Sr1(_sr1), write!(f, "delete[] [rz]")),
            OpType::NOP => opcode!(self, OpFormat::Implied, write!(f, "[00] NOP")),
        }
    }
}

impl From<&[u8]> for Instruction {
    fn from(val: &[u8]) -> Self {
        let optype = OpType::from(val[0]);
        Self {
            opcode: optype,
            data: optype.format(val),
        }
    }
}

/// VM executable Method
#[derive(Clone, PartialEq, Eq, Debug)]
pub struct VMMethod {
    pub code: Vec<Instruction>,
}

impl From<Method> for VMMethod {
    fn from(val: Method) -> Self {
        Self {
            code: (0..(val.code.len() / 4))
                .map(|i| Instruction::from(&val.code[(i * 4)..((i + 1) * 4)]))
                .collect(),
        }
    }
}

impl fmt::Display for VMMethod {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for i in self.code.iter() {
            writeln!(f, "{}", i)?;
        }
        Ok(())
    }
}
