//! RTDL MINT virtual machine
use super::constants::{OpFormat, OpType};
use std::collections::HashMap;
/// Register Set for the VM
pub struct Regset<'a> {
    regs: Vec<u32>,
    function_regs: Vec<u32>,
    pc: u32,
    lr: u32,
    srp: u32,
    sp: u32,
    stack: Vec<u32>,
    code: &'a [[u8; 4]]
}

impl<'a> Regset<'a> {
    pub fn new(code: &'a [[u8; 4]]) -> Regset<'a> {
        Regset {
            regs: Vec::new(),
            function_regs: Vec::new(),
            pc: 0,
            lr: 0,
            srp: 0,
            sp: 0,
            stack: Vec::new(),
            code
        }
    }

    fn get_sr1(&self, op: OpType, ins: &[u8]) -> u8 {
        if let OpFormat::Sr1(i) = op.format(ins) {
            return i;
        }
        unreachable!();
    }

    pub fn step(&mut self) {
        let ins = self.code[self.pc as usize];
        if ins[0] == 8 || ins[0] == 0 || ins[0] > 0x41 {
            panic!("Unknown instruction! {} {} {} {}", ins[0], ins[1], ins[2], ins[3]);
        }
        let op = OpType::from(ins[0]);
        match op {
            OpType::LDSRZR => {
                let sr1 = self.get_sr1(op, &ins);
                println!("ld b{}, false // (b{} <- false)", sr1, sr1);
                self.regs[sr1 as usize] = 0;
            },
            OpType::LDSRBT => {
                let sr1 = self.get_sr1(op, &ins);
                println!("ld b{}, true // (b{} <- true)", sr1, sr1);
                self.regs[sr1 as usize] = 1;
            },
            OpType::LDSRC4 => {

            },
            _ => {
                println!("Unimplemented");
            }
        }
    }
}
