//! Contants used by the virtual machine
#![allow(clippy::useless_attribute)] // Clippy thinks that FromPrimitive and ToPrimitive are useless

/// Opcode formats supported by this version of the interpreter
#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub enum OpFormat {
    /// Unknown opcode format 1
    Unknown(u32),
    /// Relative (code) address
    ///
    /// ## Example:
    ///
    /// `jmp +8`
    Relative(i16),
    /// Unknown opcode format 2
    Unknown2(u32),
    /// Argument + Register count
    ///
    /// ## Example:
    ///
    /// `fenter 2, 5` - 2 arguments and 5 registers in total
    ArgsRegs(u8, u8),
    /// Standard Register + Function Register
    ///
    /// ## Example:
    ///
    /// `ld fr1, r0` - passes r0 as argument 1
    Sr1Fr(u8, u8),
    /// Implied arguments
    ///
    /// ## Example:
    ///
    /// `ret r0` - `ret` ALWAYS returns r0
    Implied,
    /// One Register
    ///
    /// ## Example:
    ///
    /// `inc r0`
    Sr1(u8),
    /// One Register + Relative Code Address
    ///
    /// ## Example
    ///
    /// `jmppos r4, -12` - jumps back if r4 is positive
    Sr1Cs2(u8, i16),
    /// One Register + Constant ID
    ///
    /// ## Example:
    ///
    /// `ld r0, #5`
    Sr1Cti(u8, u16),
    /// Two Registers
    ///
    /// ## Example:
    ///
    /// `ld r0, r2`
    Sr1Sr2(u8, u8),
    /// Three Registers
    ///
    /// ## Example:
    ///
    /// `add f0, f1, f2`
    Sr1Sr2Sr3(u8, u8, u8),
    /// One Register + Static Reference
    ///
    /// ## Example:
    ///
    /// `ld r0, ["ExecInfo.RawVersion"]`
    Sr1Sti(u8, u16),
    /// Function reference
    ///
    /// ## Example:
    ///
    /// `call "Hel.Math.Vec3.abs() float"`
    Fun(u16),
}

/// List of all supported instructions
#[derive(Copy, Clone, PartialEq, Eq, Debug, FromPrimitive, ToPrimitive)]
pub enum OpType {
    /// Not an actual instruction
    NOP = 0,
    /// `ld bz, false`
    LDSRZR = 1,
    /// `ld bz, true`
    LDSRBT,
    /// `ld rz, #constant`
    LDSRC4,
    /// `ld rz, &constant`
    LDSRCA,
    /// `ld rz, rx`
    LDSRSR,
    /// `ld rz, fr0`
    LDSRFZ,
    /// `ld frz, rx`
    LDFRSR,
    /// `ld rz, ["static name"]`
    LDSRSV = 9,
    /// `ld rz, [rx]`
    LDSRA4,
    /// `ld rz, sizeof:["static name"]`
    LDSRSZ,
    /// `ld [rz], rx`
    STSRSR,
    /// `ld ["static variable"], rz
    STSVSR,
    /// `add rz, rx, ry`
    ADDI32,
    /// `sub rz, rx, ry`
    SUBI32,
    /// `muls rz, rx, ry`
    MULS32,
    /// `divs rz, rx, ry`
    DIVS32,
    /// `mods rz, rx, ry`
    MODS32,
    /// `inc rz`
    INCI32,
    /// `dec rz`
    DECI32,
    /// `neg rz, rx`
    NEGS32,
    /// `add fz, fx, fy`
    ADDF32,
    /// `sub fz, fx, fy`
    SUBF32,
    /// `mul fz, fx, fy`
    MULF32,
    /// `div fz, fx, fy`
    DIVF32,
    /// `inc fz`
    INCF32,
    /// `dec fz`
    DECF32,
    /// `neg fz, fx`
    NEGF32,
    /// `lts bz, rx, ry`
    LTS32,
    /// `les bz, rx, ry`
    LES32,
    /// `eq bz, rx, ry`
    EQI32,
    /// `ne bz, rx, ry`
    NEI32,
    /// `lt bz, fx, fy`
    LTF32,
    /// `le bz, fx, fy`
    LEF32,
    /// `eq bz, fx, fy`
    EQF32,
    /// `ne bz, fx, fy`
    NEF32,
    /// `ltcmp bz, rx`
    LTCMP,
    /// `lecmp bz, rx`
    LECMP,
    /// `eq bz, bx, by`
    EQBOOL,
    /// `ne bz, bx, by`
    NEBOOL,
    /// `and rz, rx, ry`
    ANDI32,
    /// `or rz, rx, ry`
    ORI32,
    /// `xor rz, rx, ry`
    XORI32,
    /// `not rz, rx`
    NTI32,
    /// `not bz, bx`
    NTBOOL,
    /// `lsl rz, rx, ry`
    SLLI32,
    /// `lsr rz, rx, ry`
    SLRI32,
    /// `jmp +/-off`
    JMP,
    /// `jmppos rz, +/-off`
    JMPPOS,
    /// `jmpneg rz, +/-off`
    JMPNEG,
    /// `fenter args, regs`
    FENTER,
    /// `fleave`
    FLEAVE,
    /// `fret r0`
    FRET,
    /// `call "function"`
    CALL,
    /// `yield frames`
    YIELD,
    /// `mcopy [rz], [rx], ry`
    MCOPY,
    /// `mzeros [rz], sizeof:["type name"]`
    MZEROS,
    /// `sppsh rz, "type name"`
    SPPSH,
    /// `sppshz rz, "type name"`
    SPPSHZ,
    /// `sppop [rz], "type name"`
    SPPOP,
    /// `add rz, rz, offset:"struct member"`
    ADDOFS,
    /// `arpshz rz, rz`
    ARPSHZ,
    /// `add rz, rz, offset:rx`
    ARIDX,
    /// `ld rz, sizeof:rx`
    ARLEN,
    /// `arpop [rz]`
    ARPOP,
}

impl OpType {
    /// Parses and returns the opcode format
    pub fn format(self, ins: &[u8]) -> OpFormat {
        assert_eq!(ins.len(), 4);
        assert_eq!(ins[0], self as u8);
        match self {
            OpType::LDSRZR
            | OpType::LDSRBT
            | OpType::LDSRFZ
            | OpType::INCI32
            | OpType::DECI32
            | OpType::INCF32
            | OpType::DECF32
            | OpType::YIELD
            | OpType::ARPSHZ
            | OpType::ARPOP => OpFormat::Sr1(ins[1]),
            OpType::LDSRC4 | OpType::LDSRCA => {
                OpFormat::Sr1Cti(ins[1], u16::from_be_bytes([ins[2], ins[3]]))
            }
            OpType::LDSRSR
            | OpType::LDSRA4
            | OpType::STSRSR
            | OpType::NEGS32
            | OpType::NEGF32
            | OpType::LTCMP
            | OpType::LECMP
            | OpType::NTI32
            | OpType::NTBOOL
            | OpType::ARIDX
            | OpType::ARLEN => OpFormat::Sr1Sr2(ins[1], ins[2]),
            OpType::LDFRSR => OpFormat::Sr1Fr(ins[1], ins[2]),
            OpType::LDSRSV
            | OpType::LDSRSZ
            | OpType::STSVSR
            | OpType::MZEROS
            | OpType::SPPSH
            | OpType::SPPSHZ
            | OpType::SPPOP
            | OpType::ADDOFS => OpFormat::Sr1Sti(ins[1], u16::from_be_bytes([ins[2], ins[3]])),
            OpType::ADDI32
            | OpType::SUBI32
            | OpType::MULS32
            | OpType::DIVS32
            | OpType::MODS32
            | OpType::ADDF32
            | OpType::SUBF32
            | OpType::MULF32
            | OpType::DIVF32
            | OpType::LTS32
            | OpType::LES32
            | OpType::EQI32
            | OpType::NEI32
            | OpType::LTF32
            | OpType::LEF32
            | OpType::EQF32
            | OpType::NEF32
            | OpType::EQBOOL
            | OpType::NEBOOL
            | OpType::ANDI32
            | OpType::ORI32
            | OpType::XORI32
            | OpType::SLLI32
            | OpType::SLRI32
            | OpType::MCOPY => OpFormat::Sr1Sr2Sr3(ins[1], ins[2], ins[3]),
            OpType::JMP => OpFormat::Relative(i16::from_be_bytes([ins[2], ins[3]])),
            OpType::JMPPOS | OpType::JMPNEG => {
                OpFormat::Sr1Cs2(ins[1], i16::from_be_bytes([ins[2], ins[3]]))
            }
            OpType::FENTER => OpFormat::ArgsRegs(ins[1], ins[2]),
            OpType::NOP | OpType::FLEAVE | OpType::FRET => OpFormat::Implied,
            OpType::CALL => OpFormat::Fun(u16::from_be_bytes([ins[2], ins[3]])),
        }
    }
}

impl From<u8> for OpType {
    fn from(val: u8) -> Self {
        use num_traits::FromPrimitive;
        Self::from_u8(val).unwrap()
    }
}
