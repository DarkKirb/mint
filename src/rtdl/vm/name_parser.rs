//! RTDL mint symbol name parser

/// Decodes a name into (ReturnType, FunctionName, Arguments, constant_fn)
pub fn decode<'a>(input: &'a str) -> Option<(&'a str, &'a str, Vec<&'a str>, bool)> {
    // 1) separate Return and function name from arguments and constant fn
    let pos = input.find('(')?;
    let retname = &input[..pos];
    let argsconst = &input[pos + 1..];
    let pos = retname.rfind(' ')?;
    let rettype = &retname[..pos];
    let name = &retname[pos + 1..];
    let const_off = argsconst.rfind(')')?;
    let const_fn = argsconst.len() == const_off + 1;
    Some((
        rettype,
        name,
        argsconst[..const_off].split(',').collect(),
        const_fn,
    ))
}
