//! Return to Dream Land specific features
pub mod assembler;
pub mod mintobj;
pub mod vm;
use crate::common::errors::Result;

/// This function loads a mint file (archive) from disk and calls the function passed with parsed
/// mint objects.
pub fn mint_loader<F>(fname: String, loader: F) -> Result<()>
where
    F: Fn(String, mintobj::MintHeader) + Send + Sync,
{
    use rayon::prelude::*;
    use std::fs::File;
    use std::io::prelude::*;
    use std::io::Cursor;
    let mut f = {
        let mut f = File::open(fname.clone())?;
        let mut vec = Vec::new();
        f.read_to_end(&mut vec).unwrap();
        Cursor::new(vec)
    };
    // Best case: file is a mint file
    if let Ok(binary) = mintobj::MintHeader::read(&mut f) {
        loader(fname, binary);
    } else {
        f.seek(std::io::SeekFrom::Start(0))?;
        // Now we need to load it as an AR
        let ar = crate::common::ar::Ar::load(&mut f)?;
        let data: Result<Vec<(String, mintobj::MintHeader)>> = ar
            .files
            .par_iter()
            .map(|(name, file)| {
                let mut f = Cursor::new(file.data.clone());
                Ok((name.to_string(), mintobj::MintHeader::read(&mut f)?))
            })
            .collect();
        data.unwrap()
            .into_par_iter()
            .for_each(|(name, file)| loader(name, file));
    }
    Ok(())
}
