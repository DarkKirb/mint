//! Abstract Syntax Tree used for parsing the assembly code

pub type Ast<'a> = Vec<Stmt<'a>>;

#[derive(Clone, PartialEq, Debug)]
pub enum Stmt<'a> {
    SDataSection(Vec<SDataExpr<'a>>),
    TextSection(Vec<TextExpr<'a>>),
    StringSection(Vec<&'a str>),
}

#[derive(Clone, Hash, Eq, PartialEq, Debug)]
pub enum StringData<'a> {
    Ref(&'a str),
    Heap(String),
}

#[derive(Clone, PartialEq, Debug)]
pub enum SDataExpr<'a> {
    Label(&'a str),
    StringLabel(String),
    UInt(u32),
    Int(i32),
    Float(f32),
    PascalString(&'a str),
    CString(&'a str),
}

#[derive(Clone, PartialEq, Debug)]
pub enum TextExpr<'a> {
    Object(String, Vec<ObjExpr<'a>>),
}

#[derive(Clone, PartialEq, Debug)]
pub enum ObjExpr<'a> {
    Member(String, &'a str),
    Function(
        bool,
        &'a str,
        Vec<String>,
        String,
        bool,
        Vec<FunctionExpr<'a>>,
    ),
}

#[derive(Copy, Clone, PartialEq, Debug)]
pub enum Register {
    Boolean(u8),
    Integer(u8),
    FloatingPoint(u8),
    FunctionRegister(u8),
}

#[derive(Clone, PartialEq, Debug)]
pub enum FunctionExpr<'a> {
    Label(&'a str),
    Load(LoadExpr<'a>),
    Add(AddExpr<'a>),
    Sub(Register, Register, Register),
    Mul(Register, Register, Register),
    Div(Register, Register, Register),
    Mod(Register, Register, Register),
    Inc(Register),
    Dec(Register),
    Neg(Register, Register),
    Lt(Register, Register, Register),
    Le(Register, Register, Register),
    Eq(Register, Register, Register),
    Ne(Register, Register, Register),
    LtCmp(Register, Register),
    LeCmp(Register, Register),
    And(Register, Register, Register),
    Or(Register, Register, Register),
    Xor(Register, Register, Register),
    Not(Register, Register),
    Lsl(Register, Register, Register),
    Lsr(Register, Register, Register),
    Jmp(&'a str),
    JmpOff(i16),
    JmpPos(Register, &'a str),
    JmpPosOff(Register, i16),
    JmpNeg(Register, &'a str),
    JmpNegOff(Register, i16),
    Return,
    Call(&'a str),
    CallInt(usize),
    Yield(Register),
    Copy(Register, Register, Register),
    Clear(Register, &'a str),
    ClearInt(Register, usize),
    New(Register, &'a str),
    NewInt(Register, usize),
    NewClear(Register, &'a str),
    NewClearInt(Register, usize),
    Delete(Register, &'a str),
    DeleteInt(Register, usize),
    NewArr(Register),
    DeleteArr(Register),
}

#[derive(Clone, PartialEq, Debug)]
pub enum LoadExpr<'a> {
    LoadBool(Register, bool),
    LoadConstantInt(Register, u32),
    LoadConstantFloat(Register, f32),
    LoadConstant(Register, &'a str),
    LoadConstantString(Register, String),
    LoadConstantSData(Register, u16),
    LoadConstantAddr(Register, &'a str),
    LoadConstantSDataAddr(Register, u16),
    LoadRegister(Register, Register),
    LoadResult(Register),
    LoadFR(Register, Register),
    LoadStatic(Register, &'a str),
    LoadStaticInt(Register, usize),
    LoadIndirect(Register, Register),
    LoadSize(Register, &'a str),
    LoadSizeInt(Register, usize),
    StoreReg(Register, Register),
    StoreStatic(&'a str, Register),
    StoreStaticInt(usize, Register),
    LoadLength(Register, Register),
}

#[derive(Clone, PartialEq, Debug)]
pub enum AddExpr<'a> {
    Registers(Register, Register, Register),
    Addofs(Register, &'a str),
    AddofsInt(Register, usize),
    Aridx(Register, Register),
}
