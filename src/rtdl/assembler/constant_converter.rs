//! Module responsible for converting an AST into one without constants in the code section
use super::ast::*;

/// This function merges multiple SData and Text sections together
pub fn section_merger<'a>(ast: Ast<'a>) -> Ast<'a> {
    let mut sdata_expr: Vec<SDataExpr<'a>> = Vec::new();
    let mut text_expr: Vec<TextExpr<'a>> = Vec::new();
    for stmt in ast.into_iter() {
        match stmt {
            Stmt::SDataSection(mut sdata) => sdata_expr.append(&mut sdata),
            Stmt::TextSection(mut text) => text_expr.append(&mut text),
            _ => {}
        }
    }
    vec![Stmt::SDataSection(sdata_expr), Stmt::TextSection(text_expr)]
}

pub fn constant_converter<'a>(mut ast: Ast<'a>) -> Ast<'a> {
    let mut constants: Vec<SDataExpr<'a>> = Vec::new();
    let mut label_count: usize = 0;
    for stmt in ast.iter_mut() {
        match stmt {
            Stmt::TextSection(ref mut text) => {
                for obj in text.iter_mut() {
                    match obj {
                        TextExpr::Object(_, ref mut expr) => {
                            for function in expr.iter_mut() {
                                match function {
                                    ObjExpr::Function(_, _, _, _, _, ref mut code) => {
                                        for line in code.iter_mut() {
                                            match line {
                                                FunctionExpr::Load(ref mut expr) => match expr {
                                                    LoadExpr::LoadConstantInt(reg, int) => {
                                                        let label_name = format!(
                                                            "__internal_lbl_{}",
                                                            label_count
                                                        );
                                                        label_count += 1;
                                                        constants.push(SDataExpr::StringLabel(
                                                            label_name.clone(),
                                                        ));
                                                        constants.push(SDataExpr::UInt(*int));
                                                        *expr = LoadExpr::LoadConstantString(
                                                            *reg, label_name,
                                                        );
                                                    }
                                                    LoadExpr::LoadConstantFloat(reg, float) => {
                                                        let label_name = format!(
                                                            "__internal_lbl_{}",
                                                            label_count
                                                        );
                                                        label_count += 1;
                                                        constants.push(SDataExpr::StringLabel(
                                                            label_name.clone(),
                                                        ));
                                                        constants.push(SDataExpr::Float(*float));
                                                        *expr = LoadExpr::LoadConstantString(
                                                            *reg, label_name,
                                                        );
                                                    }
                                                    _ => {}
                                                },
                                                _ => {}
                                            }
                                        }
                                    }
                                    _ => {}
                                }
                            }
                        }
                    }
                }
            }
            _ => {}
        }
    }
    ast.push(Stmt::SDataSection(constants));
    section_merger(ast)
}
