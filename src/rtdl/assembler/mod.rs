//! Assembler module for RTDL mint
//!
//! Assembly steps:
//! 1. Lexing the code into an AST (in `lex`)
//! 2. Converting constants into labels in the sdata section (in `constant_converter`)
//! 3. Converting labels into absolute/relative offset (in `relocator`)
//! 4. Constructing a binary for each section
//! 5. Linking the binary together in one file
lalrpop_mod!(#[allow(clippy::all)] pub lex, "/rtdl/assembler/lex.rs");
pub mod ast;
pub mod constant_converter;
pub mod relocator;
