//! This module is responsible for converting labels into absolute/relative offsets
//! The code generator can't work with labels
use super::ast::*;
use std::collections::HashMap;

#[allow(irrefutable_let_patterns)]
pub fn relocate_labels<'a>(mut ast: Ast<'a>) -> Ast<'a> {
    let mut sdata_labels: HashMap<StringData<'a>, usize> = HashMap::new();
    let mut strings: HashMap<&'a str, usize> = HashMap::new();
    let mut string_section: Vec<&'a str> = Vec::new();

    for stmt in ast.iter_mut() {
        match stmt {
            Stmt::SDataSection(ref sdata) => {
                let mut pos: usize = 0;
                for expr in sdata.iter() {
                    match expr {
                        SDataExpr::Label(l) => {
                            sdata_labels.insert(StringData::Ref(l), pos);
                        }
                        SDataExpr::StringLabel(l) => {
                            sdata_labels.insert(StringData::Heap(l.clone()), pos);
                        }
                        SDataExpr::UInt(_) => {
                            pos += 4;
                        }
                        SDataExpr::Int(_) => {
                            pos += 4;
                        }
                        SDataExpr::Float(_) => {
                            pos += 4;
                        }
                        SDataExpr::PascalString(s) => {
                            pos += 4 + ((s.len() + 4) / 4) * 4; // Add padding to the end of the string
                        }
                        SDataExpr::CString(s) => {
                            pos += ((s.len() + 4) / 4) * 4; // Add padding to the end of the string
                        }
                    }
                }
            }
            Stmt::TextSection(ref mut text) => {
                for expr in text.iter_mut() {
                    if let TextExpr::Object(_, ref mut object) = expr {
                        for expr in object.iter_mut() {
                            match expr {
                                ObjExpr::Function(_, _, _, _, _, ref mut code) => {
                                    let mut code_labels: HashMap<&'a str, i16> = HashMap::new();
                                    let mut pos: i16 = 0;
                                    // 1. get all code labels
                                    for line in code.iter() {
                                        match line {
                                            FunctionExpr::Label(s) => {
                                                code_labels.insert(s, pos);
                                            }
                                            _ => {
                                                pos += 1;
                                            }
                                        }
                                    }
                                    pos = 0;
                                    // 2. modify everything using labels to using actual offsets
                                    for line in code.iter_mut() {
                                        match line {
                                            FunctionExpr::Label(_) => {
                                                pos -= 1;
                                            }
                                            FunctionExpr::Jmp(s) => {
                                                *line = FunctionExpr::JmpOff(pos - code_labels[s]);
                                            }
                                            FunctionExpr::JmpPos(r, s) => {
                                                *line = FunctionExpr::JmpPosOff(
                                                    *r,
                                                    pos - code_labels[s],
                                                );
                                            }
                                            FunctionExpr::JmpNeg(r, s) => {
                                                *line = FunctionExpr::JmpNegOff(
                                                    *r,
                                                    pos - code_labels[s],
                                                );
                                            }
                                            FunctionExpr::Call(s) => {
                                                if !strings.contains_key(s) {
                                                    let id = string_section.len();
                                                    string_section.push(s);
                                                    strings.insert(s, id);
                                                }
                                                *line = FunctionExpr::CallInt(strings[s]);
                                            }
                                            FunctionExpr::Clear(r, s) => {
                                                if !strings.contains_key(s) {
                                                    let id = string_section.len();
                                                    string_section.push(s);
                                                    strings.insert(s, id);
                                                }
                                                *line = FunctionExpr::ClearInt(*r, strings[s]);
                                            }
                                            FunctionExpr::New(r, s) => {
                                                if !strings.contains_key(s) {
                                                    let id = string_section.len();
                                                    string_section.push(s);
                                                    strings.insert(s, id);
                                                }
                                                *line = FunctionExpr::NewInt(*r, strings[s]);
                                            }
                                            FunctionExpr::NewClear(r, s) => {
                                                if !strings.contains_key(s) {
                                                    let id = string_section.len();
                                                    string_section.push(s);
                                                    strings.insert(s, id);
                                                }
                                                *line = FunctionExpr::NewClearInt(*r, strings[s]);
                                            }
                                            FunctionExpr::Delete(r, s) => {
                                                if !strings.contains_key(s) {
                                                    let id = string_section.len();
                                                    string_section.push(s);
                                                    strings.insert(s, id);
                                                }
                                                *line = FunctionExpr::DeleteInt(*r, strings[s]);
                                            }
                                            FunctionExpr::Load(ref mut expr) => match expr {
                                                LoadExpr::LoadStatic(r, s) => {
                                                    if !strings.contains_key(s) {
                                                        let id = string_section.len();
                                                        string_section.push(s);
                                                        strings.insert(s, id);
                                                    }
                                                    *expr = LoadExpr::LoadStaticInt(*r, strings[s]);
                                                }
                                                LoadExpr::StoreStatic(s, r) => {
                                                    if !strings.contains_key(s) {
                                                        let id = string_section.len();
                                                        string_section.push(s);
                                                        strings.insert(s, id);
                                                    }
                                                    *expr =
                                                        LoadExpr::StoreStaticInt(strings[s], *r);
                                                }
                                                _ => {}
                                            },
                                            FunctionExpr::Add(ref mut expr) => {
                                                if let AddExpr::Addofs(r, s) = expr {
                                                    if !strings.contains_key(s) {
                                                        let id = string_section.len();
                                                        string_section.push(s);
                                                        strings.insert(s, id);
                                                    }
                                                    *expr = AddExpr::AddofsInt(*r, strings[s]);
                                                }
                                            }
                                            _ => {}
                                        }
                                        pos += 1;
                                    }
                                }
                                _ => {}
                            }
                        }
                    }
                }
            }
            _ => {}
        }
    }

    ast.push(Stmt::StringSection(string_section));
    ast
}
