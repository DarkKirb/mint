use mint::rtdl::vm::name_parser;
use parking_lot::Mutex;
use rayon::prelude::*;
use std::collections::HashSet;
use std::env;
use std::sync::Arc;

fn main() {
    let mut it = env::args();
    it.next().unwrap();
    let defined_data: Arc<Mutex<HashSet<String>>> = Arc::new(Mutex::new(HashSet::new()));
    let defined_code: Arc<Mutex<HashSet<String>>> = Arc::new(Mutex::new(HashSet::new()));
    let defined_objs: Arc<Mutex<HashSet<String>>> = Arc::new(Mutex::new(HashSet::new()));
    let requested_symbols: Arc<Mutex<HashSet<String>>> = Arc::new(Mutex::new(HashSet::new()));
    it.collect::<Vec<_>>().par_iter().for_each(|f| {
        mint::rtdl::mint_loader(f.to_string(), |_file, mint| {
            let data_arc = Arc::clone(&defined_data);
            let code_arc = Arc::clone(&defined_code);
            let obj_arc = Arc::clone(&defined_objs);
            let req_arc = Arc::clone(&requested_symbols);
            for sym in mint.symbol_list.iter() {
                req_arc.lock().insert(sym.to_string());
            }
            for obj in mint.objects.iter() {
                obj_arc.lock().insert(obj.name.clone());
                {
                    let mut data = data_arc.lock();
                    for field in obj.fields.iter() {
                        let field_name = obj.name.clone() + "." + &field.name;
                        data.insert(field_name);
                    }
                }
                for method in obj.methods.iter() {
                    let (_, name, args, const_fn): (&str, &str, Vec<&str>, bool) =
                        name_parser::decode(&method.name).unwrap();
                    let mut method_name =
                        obj.name.clone() + "." + name + "(" + &args.join(",") + ")";
                    if const_fn {
                        method_name += "const";
                    }
                    code_arc.lock().insert(method_name);
                }
            }
        })
        .unwrap();
    });
    let diff: HashSet<_> = requested_symbols
        .lock()
        .difference(&defined_data.lock())
        .map(std::string::ToString::to_string)
        .collect();
    let diff: HashSet<_> = diff
        .difference(&defined_code.lock())
        .map(std::string::ToString::to_string)
        .collect();
    for t in defined_code.lock().iter() {
        println!("t {}", t);
    }
    for d in defined_data.lock().iter() {
        println!("d {}", d);
    }
    for t in defined_objs.lock().iter() {
        println!("T {}", t);
    }
    for u in diff.difference(&defined_objs.lock()) {
        println!("U {}", u);
    }
}
