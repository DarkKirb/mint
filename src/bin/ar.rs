#[macro_use]
extern crate clap;
use mint::common::ar::Ar;
use mint::common::games::{Endian, MintVersion};
use std::collections::HashSet;
use std::fs::File;
use std::io::Write;
use std::path;

fn main() {
    let matches = clap_app!(ar =>
        (version: "0.1")
        (author: "Charlotte D. <darkkirb@darkkirb.de>")
        (about: "create, modify, and extract from archives")
        (@arg delete: -d "Deletes files")
        (@arg move: -m "Moves files")
        (@arg print: -p "Prints files")
        (@arg replace: -r "Replaces or adds files")
        (@arg ranlib: -s "Act like ranlib (does nothing right now)")
        (@arg test: -t "Display contents of the archive")
        (@arg extract: -x "Extract files from the archive")
        (@arg ARCHIVE_FILE: +required "Sets the archive file to use")
        (@arg AFTER: -a +takes_value "put file(s) after member")
        (@arg BEFORE: -i -b +takes_value "put file(s) before member")
        (@arg ignore_create: -c "do not warn if the library had to be created")
        (@arg ARCHIVE_MEMBERS: ... "Members of the archive to do things with")
        (@arg ENGINE_VERSION: -M +takes_value "Engine version to create an archive for")
    )
    .get_matches();
    let file = matches.value_of("ARCHIVE_FILE").unwrap();
    let output_exists = path::Path::new(file).exists();
    let mut modified = false;
    // Check for eplacement
    let mut archive = if matches.occurrences_of("replace") != 0 {
        if !output_exists {
            if matches.occurrences_of("ignore_create") == 0 {
                println!("WARNING: creating output file {}.", file);
            }
            let engine_ver = matches.value_of("ENGINE_VERSION").unwrap();
            let (endian, engine_ver) = match engine_ver {
                "0.2.0" => (Endian::Big, MintVersion::V0_2_0),
                _ => unimplemented!(),
            };
            modified = true;
            Ar::create(endian, engine_ver)
        } else {
            let mut f = File::open(file).unwrap();
            Ar::load(&mut f).unwrap()
        }
    } else {
        if !output_exists {
            panic!("Output file {} doesn't exist!", file);
        }
        let mut f = File::open(file).unwrap();
        Ar::load(&mut f).unwrap()
    };

    if matches.occurrences_of("delete") != 0 {
        for item in matches.values_of("ARCHIVE_MEMBERS").unwrap() {
            if let Err(e) = archive.delete(item.to_string()) {
                println!("WARNING: Could not delete {}: {}", item, e);
            } else {
                modified = true;
            }
        }
    }

    if matches.occurrences_of("move") != 0 {
        let after = matches.value_of("AFTER");
        let before = matches.value_of("BEFORE");
        for item in matches.values_of("ARCHIVE_MEMBERS").unwrap() {
            let o = if let Some(s) = after {
                archive.move_after(item.to_string(), s.to_string())
            } else if let Some(s) = before {
                archive.move_before(item.to_string(), s.to_string())
            } else {
                panic!("You need to pass -a or -b!");
            };
            if let Err(e) = o {
                println!("WARNING: Could not move {}: {}", item, e);
            } else {
                modified = true;
            }
        }
    }

    if matches.occurrences_of("print") != 0 || matches.occurrences_of("test") != 0 {
        let archive_files: HashSet<_> = archive.order.iter().cloned().collect();
        let args_files = matches
            .values_of("ARCHIVE_MEMBERS")
            .map(|x| x.map(std::string::ToString::to_string).collect())
            .unwrap_or_else(|| archive_files.clone());
        for file in archive_files.intersection(&args_files) {
            println!("{}", file);
        }
    }

    if matches.occurrences_of("replace") != 0 {
        for item in matches.values_of("ARCHIVE_MEMBERS").unwrap() {
            let mut f = File::open(item).unwrap();
            archive.replace(item.to_string(), &mut f).unwrap();
            modified = true;
        }
    }

    if matches.occurrences_of("extract") != 0 {
        let archive_files: HashSet<_> = archive.order.iter().cloned().collect();
        let args_files = matches
            .values_of("ARCHIVE_MEMBERS")
            .map(|x| x.map(std::string::ToString::to_string).collect())
            .unwrap_or_else(|| archive_files.clone());
        for file in archive_files.intersection(&args_files) {
            let mut f = File::create(file).unwrap();
            f.write_all(&archive.files[file].data).unwrap();
        }
    }

    if modified {
        let mut f = File::create(file).unwrap();
        archive.write(&mut f).unwrap();
    }
}
