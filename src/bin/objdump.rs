use itertools::Itertools;
use mint::rtdl::{
    mintobj,
    vm::{constants::OpFormat, decoder, name_parser},
};
use parking_lot::Mutex;
use rayon::prelude::*;
use std::env;
use std::path;
use std::sync::Arc;

fn disassemble(file: String, mint: mintobj::MintHeader, lines: Arc<Mutex<Vec<Vec<String>>>>) {
    let mut output: Vec<String> = Vec::new();
    output.push(format!("Input file: {}", file));
    output.push(format!(
        "File information: Endian: {:?}, Name: {}",
        mint.header.endian, mint.name
    ));
    output.push(format!("Mint Version: {:?}", mint.version));
    output.push("".to_string());
    output.push("In section .sdata".to_string());
    output.push("WARNING: Unknown data format".to_string());
    for line in hexdump::hexdump_iter(&mint.sdata) {
        output.push(line.to_string());
    }
    output.push("".to_string());
    output.push("In section .syms".to_string());
    for (i, sym) in mint.symbol_list.iter().enumerate() {
        output.push(format!("{}: {}", i, sym));
    }
    output.push("".to_string());
    output.push("In section .objects".to_string());
    for obj in mint.objects.iter() {
        output.push(format!(".struct {}", obj.name));
        for field in obj.fields.iter() {
            output.push(format!("    {}: {}", field.name, field.r#type));
        }
        output.push(".endstruct".to_string());
    }
    output.push("".to_string());
    output.push("In section .text".to_string());
    for obj in mint.objects.iter() {
        for method in obj.methods.iter() {
            // Parse the name
            let (return_type, parsed_name, args, const_fn): (&str, &str, Vec<&str>, bool) =
                name_parser::decode(&method.name).unwrap();
            if const_fn {
                let s = format!("{}.{}({})const: ", obj.name, parsed_name, args.join(", "));
                output.push(
                    s + &format!(
                        "(aka {} {}({})const of object {})",
                        return_type,
                        parsed_name,
                        args.join(", "),
                        obj.name
                    ),
                );
            } else {
                let s = format!("{}.{}({}): ", obj.name, parsed_name, args.join(", "));
                output.push(
                    s + &format!(
                        "(aka {} {}({}) of object {})",
                        return_type,
                        parsed_name,
                        args.join(", "),
                        obj.name
                    ),
                );
            }
            // Loop over every instruction and print it out.
            let vmmethod: decoder::VMMethod = method.clone().into();
            for (i, ins) in vmmethod.code.iter().enumerate() {
                let s = format!("{:<8}{}  \t", i, ins);
                let s2 = match ins.data {
                    OpFormat::Relative(off) => {
                        format!("(-> {})", i.wrapping_add((off as isize) as usize))
                    }
                    OpFormat::Sr1Cs2(_, off) => {
                        format!("(-> {})", i.wrapping_add((off as isize) as usize))
                    }
                    OpFormat::Sr1Cti(_, cti) => {
                        let off = cti as usize;
                        let constant = u32::from_be_bytes([
                            mint.sdata[off],
                            mint.sdata[off + 1],
                            mint.sdata[off + 2],
                            mint.sdata[off + 3],
                        ]);
                        format!("(constant value 0x{:x})", constant)
                    }
                    OpFormat::Sr1Sti(_, sti) => {
                        format!("(name: {})", mint.symbol_list[sti as usize])
                    }
                    OpFormat::Fun(sti) => format!("(name: {})", mint.symbol_list[sti as usize]),
                    _ => "".to_string(),
                };
                output.push(s + &s2);
            }
        }
    }
    lines.lock().push(output);
}

fn main() {
    let mut it = env::args();
    it.next().unwrap();
    let lines: Arc<Mutex<Vec<Vec<String>>>> = Arc::new(Mutex::new(Vec::new()));
    it.collect::<Vec<_>>().par_iter().for_each(|file| {
        assert!(path::Path::new(file).exists());
        mint::rtdl::mint_loader(file.to_string(), |file, mint| {
            disassemble(file, mint, Arc::clone(&lines));
        })
        .unwrap();
    });
    print!("{}", lines.lock().iter().map(|s| s.join("\n")).join("\n"));
}
