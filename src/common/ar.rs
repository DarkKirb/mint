//! XBIN ARchive
//!
//! File type used by rtdl and tdx (and others) to store multiple XBIN files compressed in a single
//! file.
//!
//! The actual file format is simple. After the header the archive contains a member count. After
//! that offsets to (name, data) pairs. The size of the members can be read from the XBIN header
//! of each file

use super::errors::{MintError, Result};
use super::games::MintVersion;
use super::io::{SeekReadable, SeekWriteable};
use super::xbin::{Endian, XBIN};
use std::collections::HashMap;
use std::io::{Read, Seek, SeekFrom, Write};

/// XBIN Archive member. This is used internally in the Ar struct.
#[derive(Clone, PartialEq, Eq, Debug)]
pub struct ArchiveMember {
    pub header: XBIN,
    pub data: Vec<u8>,
}

impl super::io::SeekReadable for ArchiveMember {
    fn read<R>(reader: &mut R, e: Endian) -> Result<Self>
    where
        R: Read + Seek + byteorder::ReadBytesExt,
    {
        let pos = reader.seek(SeekFrom::Current(0))?;
        let xbin = XBIN::load(reader)?;
        reader.seek(SeekFrom::Start(pos))?;
        assert_eq!(xbin.endian, e);
        let mut data: Vec<_> = (0..xbin.size).map(|_| 0u8).collect();
        reader.read_exact(&mut data)?;
        Ok(ArchiveMember {
            header: xbin,
            data: data,
        })
    }
}

impl super::io::Writeable for ArchiveMember {
    fn write<W>(&self, write: &mut W, _: Endian) -> Result<()>
    where
        W: Write + byteorder::WriteBytesExt,
    {
        self.header.write(write, &self.data)?;
        Ok(())
    }
}

/// XBIN Archive file
#[derive(Clone, PartialEq, Eq, Debug)]
pub struct Ar {
    /// Header of the Archive. Should not be touched under normal circumstances
    pub header: XBIN,
    /// Files in the archive
    pub files: HashMap<String, ArchiveMember>,
    /// Order of the files in the archive.
    pub order: Vec<String>,
}

impl Ar {
    pub fn create(endian: Endian, engine_ver: MintVersion) -> Ar {
        let id = match engine_ver {
            MintVersion::V0_2_0 => 0xfde9,
            _ => unimplemented!(),
        };
        // Ar is only supported with v2 archives afaik
        Ar {
            header: XBIN::create_v2(endian, id),
            files: HashMap::new(),
            order: Vec::new(),
        }
    }
    /// Reads the Ar file from a reader. If the reader points to valid data, it will consume the
    /// entire Ar file. If the reader does not point to valid data, the reader will be left in
    /// undefined state.
    pub fn load<R>(reader: &mut R) -> Result<Ar>
    where
        R: Read + Seek,
    {
        let header = XBIN::load(reader)?;
        match header.kind {
            0xfde9 => (),
            _ => unimplemented!(),
        }
        let files = <HashMap<String, ArchiveMember> as SeekReadable>::read(reader, header.endian)?;
        Ok(Ar {
            header,
            order: files.iter().map(|(k, _)| k.clone()).collect(),
            files,
        })
    }

    /// Deletes a file from an archive
    pub fn delete(&mut self, fname: String) -> Result<()> {
        if !self.files.contains_key(&fname) {
            return Err(MintError::FileNotFound(fname));
        }
        self.files.remove(&fname);
        let i = self
            .order
            .iter()
            .enumerate()
            .find(|(_, v)| *v == &fname)
            .expect("Internal consistency")
            .0;
        self.order.remove(i);
        Ok(())
    }

    /// Moves a file after another file
    pub fn move_after(&mut self, fname: String, reference: String) -> Result<()> {
        if !self.files.contains_key(&fname) {
            return Err(MintError::FileNotFound(fname));
        }
        if !self.files.contains_key(&reference) {
            return Err(MintError::FileNotFound(reference));
        }
        let fname_index = self
            .order
            .iter()
            .enumerate()
            .find(|(_, v)| *v == &fname)
            .expect("Internal consistency")
            .0;
        let tmp = self.order.remove(fname_index);
        let reference_index = self
            .order
            .iter()
            .enumerate()
            .find(|(_, v)| *v == &reference)
            .expect("Internal consistency")
            .0;
        self.order.insert(reference_index + 1, tmp);
        Ok(())
    }
    /// Moves a file before another file
    pub fn move_before(&mut self, fname: String, reference: String) -> Result<()> {
        if !self.files.contains_key(&fname) {
            return Err(MintError::FileNotFound(fname));
        }
        if !self.files.contains_key(&reference) {
            return Err(MintError::FileNotFound(reference));
        }
        let fname_index = self
            .order
            .iter()
            .enumerate()
            .find(|(_, v)| *v == &fname)
            .expect("Internal consistency")
            .0;
        let reference_index = self
            .order
            .iter()
            .enumerate()
            .find(|(_, v)| *v == &reference)
            .expect("Internal consistency")
            .0;
        let tmp = self.order.remove(fname_index);
        self.order.insert(reference_index, tmp);
        Ok(())
    }

    /// Replaces or adds a file to the archive
    pub fn replace<R>(&mut self, fname: String, reader: &mut R) -> Result<()>
    where
        R: Read + Seek,
    {
        if !self.files.contains_key(&fname) {
            // Add file to archive
            self.order.push(fname.clone());
        }
        let fh = XBIN::load(reader)?;
        reader.seek(SeekFrom::Start(0))?;
        let mut buf = Vec::new();
        reader.read_to_end(&mut buf)?;
        self.files.insert(
            fname,
            ArchiveMember {
                header: fh,
                data: buf,
            },
        );
        Ok(())
    }

    /// Saves the archive to a writer
    pub fn write<W>(&self, writer: &mut W) -> Result<()>
    where
        W: Write + Seek,
    {
        self.header.write(writer, &[])?;
        self.files.write(writer, self.header.endian)?;
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn creation() {
        let ar = Ar::create(Endian::Big, MintVersion::V0_2_0);
    }
    #[test]
    #[should_panic]
    fn test_illegal_parm() {
        Ar::create(Endian::Little, MintVersion::V2_1_4);
    }
    #[test]
    fn load_file() {
        use std::io::Cursor;
        let mut reader = Cursor::new(            &b"XBIN\x12\x34\x02\x00\x00\x00\x00\x38\x00\x00\xfd\xe9\x00\x00\x00\x01\x00\x00\x00\x1c\x00\x00\x00\x28\x00\x00\x00\x04test\0\0\0\0XBIN\x12\x34\x02\x00\x00\x00\x00\x10\x00\x00\x00\x00"[..]
        );
        Ar::load(&mut reader).unwrap();
        let mut reader = Cursor::new( &b"XBIN\x34\x12\x02\x00\x38\x00\x00\x00\xe9\xfd\x00\x00\x01\x00\x00\x00\x1c\x00\x00\x00\x28\x00\x00\x00\x04\x00\x00\x00test\0\0\0\0XBIN\x34\x12\x02\x00\x10\x00\x00\x00\x00\x00\x00\x00"[..]
        );
        Ar::load(&mut reader).unwrap();
    }
    #[test]
    #[should_panic]
    fn load_broken_file() {
        use std::io::Cursor;
        let mut reader = Cursor::new(            &b"XBIN\x12\x34\x02\x00\x00\x00\x00\x38\x00\x00\xfd\xea\x00\x00\x00\x01\x00\x00\x00\x1c\x00\x00\x00\x28\x00\x00\x00\x04test\0\0\0\0XBIN\x12\x34\x02\x00\x00\x00\x00\x10\x00\x00\x00\x00"[..]
        );
        Ar::load(&mut reader).unwrap();
    }

    #[test]
    fn test_delete() {
        use std::io::Cursor;
        let mut reader = Cursor::new(            &b"XBIN\x12\x34\x02\x00\x00\x00\x00\x38\x00\x00\xfd\xe9\x00\x00\x00\x01\x00\x00\x00\x1c\x00\x00\x00\x28\x00\x00\x00\x04test\0\0\0\0XBIN\x12\x34\x02\x00\x00\x00\x00\x10\x00\x00\x00\x00"[..]
        );
        let mut ar = Ar::load(&mut reader).unwrap();
        assert!(ar.delete("test".to_string()).is_ok());
        assert!(ar.delete("test".to_string()).is_err());
    }

    #[test]
    fn test_replace() {
        use std::io::Cursor;
        let mut ar = Ar::create(Endian::Big, MintVersion::V0_2_0);
        let mut reader = Cursor::new(&b"XBIN\x12\x34\x02\x00\x00\x00\x00\x10\x00\x00\x00\x00"[..]);
        ar.replace("test".to_string(), &mut reader).unwrap();
        let mut reader = Cursor::new(&b"XBIN\x12\x34\x02\x00\x00\x00\x00\x10\x00\x00\x00\x00"[..]);
        ar.replace("test".to_string(), &mut reader).unwrap();
        let mut reader = Cursor::new(&b"XBIN\x12\x34\x02\x00\x00\x00\x00\x10\x00\x00\x00\x00"[..]);
        ar.replace("test2".to_string(), &mut reader).unwrap();
        assert!(ar
            .move_after("test".to_string(), "test2".to_string())
            .is_ok());
        assert!(ar
            .move_before("test".to_string(), "test2".to_string())
            .is_ok());
        assert!(ar
            .move_after("test3".to_string(), "test2".to_string())
            .is_err());
        assert!(ar
            .move_before("test3".to_string(), "test2".to_string())
            .is_err());
        assert!(ar
            .move_after("test".to_string(), "test3".to_string())
            .is_err());
        assert!(ar
            .move_before("test".to_string(), "test3".to_string())
            .is_err());
        let out: Vec<u8> = Vec::new();
        let mut f = Cursor::new(out);
        ar.write(&mut f).unwrap();
        let mut ar = Ar::create(Endian::Little, MintVersion::V0_2_0);
        let mut reader = Cursor::new(&b"XBIN\x12\x34\x02\x00\x00\x00\x00\x10\x00\x00\x00\x00"[..]);
        ar.replace("test".to_string(), &mut reader).unwrap();
        ar.write(&mut f).unwrap();
    }
}
