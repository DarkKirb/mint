//! IO routines used by the remainder of the code

use super::errors::{MintError, Result};
pub use super::games::Endian;
use byteorder::{BigEndian, LittleEndian, ReadBytesExt, WriteBytesExt};
use std::collections::HashMap;
use std::io::{Read, Seek, SeekFrom, Write};

/// Trait used by everything that can be read without seek
pub trait Readable: Sized {
    fn read<R>(read: &mut R, e: Endian) -> Result<Self>
    where
        R: Read + ReadBytesExt;
}

/// Trait used by everything that can be read with seeking
pub trait SeekReadable: Sized {
    fn read<R>(read: &mut R, e: Endian) -> Result<Self>
    where
        R: Read + Seek + ReadBytesExt;
}

impl<T> SeekReadable for T
where
    T: Readable,
{
    fn read<R>(read: &mut R, e: Endian) -> Result<Self>
    where
        R: Read + Seek + ReadBytesExt,
    {
        <Self as Readable>::read(read, e)
    }
}

impl Readable for u8 {
    fn read<R>(read: &mut R, _: Endian) -> Result<Self>
    where
        R: Read + ReadBytesExt,
    {
        Ok(read.read_u8()?)
    }
}

impl Readable for u16 {
    fn read<R>(read: &mut R, e: Endian) -> Result<Self>
    where
        R: Read + ReadBytesExt,
    {
        Ok(match e {
            Endian::Big => read.read_u16::<BigEndian>(),
            Endian::Little => read.read_u16::<LittleEndian>(),
        }?)
    }
}

impl Readable for u32 {
    fn read<R>(read: &mut R, e: Endian) -> Result<Self>
    where
        R: Read + ReadBytesExt,
    {
        Ok(match e {
            Endian::Big => read.read_u32::<BigEndian>(),
            Endian::Little => read.read_u32::<LittleEndian>(),
        }?)
    }
}

impl Readable for f32 {
    fn read<R>(read: &mut R, e: Endian) -> Result<Self>
    where
        R: Read + ReadBytesExt,
    {
        Ok(match e {
            Endian::Big => read.read_f32::<BigEndian>(),
            Endian::Little => read.read_f32::<LittleEndian>(),
        }?)
    }
}

impl Readable for bool {
    fn read<R>(read: &mut R, e: Endian) -> Result<Self>
    where
        R: Read + ReadBytesExt,
    {
        let val = <u32 as Readable>::read(read, e)?;
        match val {
            0 => Ok(false),
            1 => Ok(true),
            i => Err(MintError::InvalidBool(i)),
        }
    }
}

impl Readable for String {
    fn read<R>(read: &mut R, e: Endian) -> Result<Self>
    where
        R: Read + ReadBytesExt,
    {
        let length = <u32 as Readable>::read(read, e)?;
        let mut buf: Vec<_> = (0..length).map(|_| 0u8).collect();
        read.read_exact(&mut buf)?;
        Ok(String::from_utf8(buf)?)
    }
}

impl Readable for (u32, u32) {
    fn read<R>(read: &mut R, e: Endian) -> Result<Self>
    where
        R: Read + ReadBytesExt,
    {
        let a = <u32 as Readable>::read(read, e)?;
        let b = <u32 as Readable>::read(read, e)?;
        Ok((a, b))
    }
}

impl<T> SeekReadable for Vec<T>
where
    T: SeekReadable,
{
    fn read<R>(read: &mut R, e: Endian) -> Result<Self>
    where
        R: Read + Seek + ReadBytesExt,
    {
        let length = <u32 as Readable>::read(read, e)?;
        let offs = (0..length)
            .map(|_| Ok(<u32 as Readable>::read(read, e)?))
            .collect::<Result<Vec<u32>>>()?;
        let mut data: Self = Vec::new();
        for off in offs.iter() {
            read.seek(SeekFrom::Start(*off as u64))?;
            data.push(<T as SeekReadable>::read(read, e)?);
        }
        Ok(data)
    }
}

impl<K, V> SeekReadable for HashMap<K, V>
where
    K: SeekReadable + Eq + std::hash::Hash,
    V: SeekReadable,
{
    fn read<R>(read: &mut R, e: Endian) -> Result<Self>
    where
        R: Read + Seek + ReadBytesExt,
    {
        let length = <u32 as Readable>::read(read, e)?;
        let offs = (0..length)
            .map(|_| Ok(<(u32, u32) as Readable>::read(read, e)?))
            .collect::<Result<Vec<(u32, u32)>>>()?;
        let mut data: Self = HashMap::new();
        for (koff, voff) in offs.iter() {
            read.seek(SeekFrom::Start(*koff as u64))?;
            let key = <K as SeekReadable>::read(read, e)?;
            read.seek(SeekFrom::Start(*voff as u64))?;
            let val = <V as SeekReadable>::read(read, e)?;
            data.insert(key, val);
        }
        Ok(data)
    }
}

/// Trait used by everything that can be written without seek
pub trait Writeable: Sized {
    fn write<W>(&self, write: &mut W, e: Endian) -> Result<()>
    where
        W: Write + WriteBytesExt;
}

/// Trait used by everything that can be written with seeking
pub trait SeekWriteable: Sized {
    fn write<W>(&self, write: &mut W, e: Endian) -> Result<()>
    where
        W: Write + Seek + WriteBytesExt;
}

impl<T> SeekWriteable for T
where
    T: Writeable,
{
    fn write<W>(&self, write: &mut W, e: Endian) -> Result<()>
    where
        W: Write + Seek + WriteBytesExt,
    {
        Writeable::write(self, write, e)
    }
}

impl Writeable for u8 {
    fn write<W>(&self, write: &mut W, _: Endian) -> Result<()>
    where
        W: Write + WriteBytesExt,
    {
        write.write_u8(*self)?;
        Ok(())
    }
}
impl Writeable for u16 {
    fn write<W>(&self, write: &mut W, e: Endian) -> Result<()>
    where
        W: Write + WriteBytesExt,
    {
        match e {
            Endian::Big => write.write_u16::<BigEndian>(*self),
            Endian::Little => write.write_u16::<LittleEndian>(*self),
        }?;
        Ok(())
    }
}
impl Writeable for u32 {
    fn write<W>(&self, write: &mut W, e: Endian) -> Result<()>
    where
        W: Write + WriteBytesExt,
    {
        match e {
            Endian::Big => write.write_u32::<BigEndian>(*self),
            Endian::Little => write.write_u32::<LittleEndian>(*self),
        }?;
        Ok(())
    }
}
impl Writeable for f32 {
    fn write<W>(&self, write: &mut W, e: Endian) -> Result<()>
    where
        W: Write + WriteBytesExt,
    {
        match e {
            Endian::Big => write.write_f32::<BigEndian>(*self),
            Endian::Little => write.write_f32::<LittleEndian>(*self),
        }?;
        Ok(())
    }
}
impl Writeable for bool {
    fn write<W>(&self, write: &mut W, e: Endian) -> Result<()>
    where
        W: Write + WriteBytesExt,
    {
        if *self {
            Writeable::write(&0u32, write, e)
        } else {
            Writeable::write(&1u32, write, e)
        }
    }
}
impl Writeable for String {
    fn write<W>(&self, write: &mut W, e: Endian) -> Result<()>
    where
        W: Write + WriteBytesExt,
    {
        Writeable::write(&(self.len() as u32), write, e)?;
        write.write_all(self.as_bytes())?;
        let padding = [0u8; 8];
        write.write_all(&padding[(4 - self.len() % 4)..])?;
        Ok(())
    }
}
impl Writeable for (u32, u32) {
    fn write<W>(&self, write: &mut W, e: Endian) -> Result<()>
    where
        W: Write + WriteBytesExt,
    {
        Writeable::write(&self.0, write, e)?;
        Writeable::write(&self.1, write, e)
    }
}
impl<T> SeekWriteable for Vec<T>
where
    T: SeekWriteable,
{
    fn write<W>(&self, write: &mut W, e: Endian) -> Result<()>
    where
        W: Write + Seek + WriteBytesExt,
    {
        Writeable::write(&(self.len() as u32), write, e)?;
        let cur = write.seek(SeekFrom::Current(0))?;
        let tmpbuf: Vec<_> = (0..self.len() * 4).map(|_| 0u8).collect();
        write.write_all(&tmpbuf)?;
        for (i, elem) in self.iter().enumerate() {
            let pos = write.seek(SeekFrom::End(0))?;
            write.seek(SeekFrom::Start(cur + 4 * i as u64))?;
            Writeable::write(&(pos as u32), write, e)?;
            write.seek(SeekFrom::Start(pos))?;
            SeekWriteable::write(elem, write, e)?;
        }
        Ok(())
    }
}

impl<K, V> SeekWriteable for HashMap<K, V>
where
    K: SeekWriteable + Eq + std::hash::Hash,
    V: SeekWriteable,
{
    fn write<W>(&self, write: &mut W, e: Endian) -> Result<()>
    where
        W: Write + Seek + WriteBytesExt,
    {
        Writeable::write(&(self.len() as u32), write, e)?;
        let cur = write.seek(SeekFrom::Current(0))?;
        let tmpbuf: Vec<_> = (0..self.len() * 8).map(|_| 0u8).collect();
        write.write_all(&tmpbuf)?;
        for (i, (k, v)) in self.iter().enumerate() {
            let pos = write.seek(SeekFrom::End(0))?;
            write.seek(SeekFrom::Start(cur + 8 * i as u64))?;
            Writeable::write(&(pos as u32), write, e)?;
            write.seek(SeekFrom::Start(pos))?;
            SeekWriteable::write(k, write, e)?;
            let pos = write.seek(SeekFrom::End(0))?;
            write.seek(SeekFrom::Start(cur + 8 * i as u64 + 4))?;
            Writeable::write(&(pos as u32), write, e)?;
            write.seek(SeekFrom::Start(pos))?;
            SeekWriteable::write(v, write, e)?;
        }
        Ok(())
    }
}
