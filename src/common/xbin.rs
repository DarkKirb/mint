//! XBIN file format reader/writer
use super::errors::{MintError, Result};
pub use super::games::{Endian, XBinVersion};
use super::io::Writeable;
use byteorder::{BigEndian, ByteOrder, LittleEndian};
use std::io::{Read, Write};

/// XBIN file format
#[derive(Clone, PartialEq, Eq, Debug)]
pub struct XBIN {
    /// Endian of the XBIN file, for reconstruction
    pub endian: Endian,
    /// Version. Currently only version 2.0 and 4.0 supported
    pub version: XBinVersion,
    /// Type of XBIN. Every game has different values for these
    pub kind: u32,
    /// Size of XBIN
    pub size: u32,
}

impl XBIN {
    /// Creates a new V2 XBIN
    pub fn create_v2(endian: Endian, kind: u32) -> XBIN {
        XBIN {
            endian,
            version: XBinVersion::V2,
            kind,
            size: 0,
        }
    }

    /// Creates a new V4 XBIN
    pub fn create_v4(endian: Endian, kind: u32, uid: Option<u32>) -> XBIN {
        XBIN {
            endian,
            version: XBinVersion::V4(uid.unwrap_or_else(rand::random)),
            kind,
            size: 0,
        }
    }

    /// Reads the XBIN header from a reader. If the reader points to a valid XBIN header, it will
    /// be left off where the data starts (you can start reading the data). If the reader does not
    /// point to valid XBIN data, the reader will be in undefined state.
    pub fn load<R>(reader: &mut R) -> Result<XBIN>
    where
        R: Read,
    {
        let mut header = [0u8; 16];
        reader.read_exact(&mut header)?;
        if &header[0..4] != b"XBIN" {
            return Err(MintError::InvalidHeader);
        }
        let endian = match &header[4..6] {
            b"\x12\x34" => Endian::Big,
            b"\x34\x12" => Endian::Little,
            _ => {
                return Err(MintError::InvalidHeader);
            }
        };
        let version = match (header[6], header[7]) {
            (2, 0) => XBinVersion::V2,
            (4, 0) => XBinVersion::V4({
                let mut buf = [0u8; 4];
                reader.read_exact(&mut buf)?;
                match endian {
                    Endian::Big => unreachable!(), // HEL 1.1.0+ is LE-only
                    Endian::Little => LittleEndian::read_u32(&buf[..]),
                }
            }),
            _ => {
                return Err(MintError::UnsupportedVersion);
            }
        };
        let size = match endian {
            Endian::Big => BigEndian::read_u32(&header[8..12]),
            Endian::Little => LittleEndian::read_u32(&header[8..12]),
        };
        let kind = match endian {
            Endian::Big => BigEndian::read_u32(&header[12..16]),
            Endian::Little => LittleEndian::read_u32(&header[12..16]),
        };
        Ok(XBIN {
            endian,
            version,
            kind,
            size,
        })
    }

    pub fn write<W>(&self, writer: &mut W, data: &[u8]) -> Result<()>
    where
        W: Write,
    {
        writer.write_all(b"XBIN")?;
        match self.endian {
            Endian::Big => {
                writer.write_all(b"\x12\x34")?;
            }
            Endian::Little => {
                writer.write_all(b"\x34\x12")?;
            }
        }
        let version: (u8, u8) = self.version.into();
        writer.write_all(&[version.0, version.1])?;
        let len = match self.version {
            XBinVersion::V2 => 0x10,
            XBinVersion::V4(_) => 0x14,
        } + data.len() as u32;
        len.write(writer, self.endian)?;
        self.kind.write(writer, self.endian)?;
        if let XBinVersion::V4(uid) = self.version {
            uid.write(writer, self.endian)?;
        }
        writer.write_all(data)?;
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn trivial_create() {
        XBIN::create_v2(Endian::Big, 0);
        XBIN::create_v4(Endian::Little, 0, Some(1));
        XBIN::create_v4(Endian::Little, 0, None);
    }

    #[test]
    fn read_valid_xbin_data() {
        let mut d = &b"XBIN\x12\x34\x02\x00\x00\x00\x00\x10\x12\x34\x56\x78"[..];
        assert_eq!(
            XBIN::load(&mut d).unwrap(),
            XBIN {
                endian: Endian::Big,
                version: XBinVersion::V2,
                kind: 0x12345678,
                size: 0x10
            }
        );
        let mut d = &b"XBIN\x34\x12\x04\x00\x14\x00\x00\x00\x12\x34\x56\x78\x01\x00\x00\x00"[..];
        assert_eq!(
            XBIN::load(&mut d).unwrap(),
            XBIN {
                endian: Endian::Little,
                version: XBinVersion::V4(1),
                kind: 0x78563412,
                size: 0x14
            }
        );
    }

    #[test]
    fn invalid_xbin_data() {
        let mut d = &b"XUBI\0\0\0\0\0\0\0\0\0\0\0\0"[..];
        assert!(XBIN::load(&mut d).is_err());
        let mut d = &b"XBIN\x11\x22\0\0\0\0\0\0\0\0\0\0"[..];
        assert!(XBIN::load(&mut d).is_err());
        let mut d = &b"XBIN\x12\x34\x03\x00\x00\x00\x00\x10\x12\x34\x56\x78"[..];
        assert!(XBIN::load(&mut d).is_err());
        let mut d = &b"XBIN\x12\x34\x02\x00\x00\x00\x00\x10\x12\x34\x56"[..];
        assert!(XBIN::load(&mut d).is_err());
        let mut d = &b"XBIN\x34\x12\x04\x00\x14\x00\x00\x00\x12\x34\x56\x78\x01\x00\x00"[..];
        assert!(XBIN::load(&mut d).is_err());
    }

    #[test]
    fn write_xbin_data() {
        let d = b"Test data.";
        let mut v = Vec::new();
        assert!(XBIN::create_v2(Endian::Big, 0)
            .write(&mut v, &d[..])
            .is_ok());
        assert!(XBIN::create_v4(Endian::Little, 0, None)
            .write(&mut v, &d[..])
            .is_ok());
    }
}
