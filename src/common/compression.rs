//! This module transparently handles compression
use super::errors::{Result, MintError};

/// Decompress a buffer
pub fn decompress(
