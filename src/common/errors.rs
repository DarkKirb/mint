//! Error module

use std::fmt;

pub type Result<T> = std::result::Result<T, MintError>;

/// Main Error Module for this library
#[derive(Debug)]
pub enum MintError {
    IoError(std::io::Error),
    InvalidHeader,
    UnsupportedVersion,
    EncodingError(std::string::FromUtf8Error),
    FileNotFound(String),
    InvalidBool(u32),
}

impl std::error::Error for MintError {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match self {
            MintError::IoError(ref e) => Some(e),
            MintError::EncodingError(ref e) => Some(e),
            _ => None,
        }
    }
}

impl fmt::Display for MintError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            MintError::IoError(ref e) => write!(f, "IO Error occurred: {}", e),
            MintError::InvalidHeader => write!(f, "Invalid header"),
            MintError::UnsupportedVersion => write!(f, "Unsupported Version"),
            MintError::EncodingError(ref e) => write!(f, "Encoding Error occurred: {}", e),
            MintError::FileNotFound(ref s) => write!(f, "File {} not found in archive.", s),
            MintError::InvalidBool(ref i) => write!(f, "Invalid bool`with value {}", i),
        }
    }
}

impl From<std::io::Error> for MintError {
    fn from(src: std::io::Error) -> MintError {
        MintError::IoError(src)
    }
}

impl From<std::string::FromUtf8Error> for MintError {
    fn from(src: std::string::FromUtf8Error) -> MintError {
        MintError::EncodingError(src)
    }
}
