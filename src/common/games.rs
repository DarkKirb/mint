//! Enums containing versions of various things used by the engine

/// Endian enum
#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub enum Endian {
    Big,
    Little,
}
/// Systems supported by HEL
#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub enum System {
    Wii,
    WiiU,
    N3DS,
    Switch,
    Android,
}

impl From<System> for Endian {
    fn from(system: System) -> Self {
        match system {
            System::Wii | System::WiiU => Endian::Big,
            _ => Endian::Little,
        }
    }
}

/// Games using the HEL engine. Keep in mind that not all of them are supported.
#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub enum Game {
    ReturnToDreamLand,
    AnniversaryCollection,
    TripleDeluxe,
    RainbowCurse,
    FightersDeluxe,
    DrumDashDeluxe,
    BoxBoy,
    PlanetRobobot,
    BoxBoxBoy,
    BlowoutBlast,
    TeamKirbyClashDeluxe,
    ByeByeBoxBoy,
    BattleRoyale,
    PartTimeUfo,
    StarAllies,
}

impl From<Game> for System {
    fn from(game: Game) -> System {
        match game {
            Game::ReturnToDreamLand | Game::AnniversaryCollection => System::Wii,
            Game::TripleDeluxe
            | Game::FightersDeluxe
            | Game::DrumDashDeluxe
            | Game::BoxBoy
            | Game::PlanetRobobot
            | Game::BoxBoxBoy
            | Game::BlowoutBlast
            | Game::TeamKirbyClashDeluxe
            | Game::ByeByeBoxBoy
            | Game::BattleRoyale => System::N3DS,
            Game::RainbowCurse => System::WiiU,
            Game::PartTimeUfo => System::Android,
            Game::StarAllies => System::Switch,
        }
    }
}

impl From<Game> for MintVersion {
    fn from(game: Game) -> MintVersion {
        match game {
            Game::ReturnToDreamLand | Game::AnniversaryCollection => MintVersion::V0_2_0,
            Game::TripleDeluxe => MintVersion::V1_0_5,
            Game::FightersDeluxe | Game::DrumDashDeluxe => MintVersion::V1_0_8,
            Game::BoxBoy => MintVersion::V1_0_13,
            Game::RainbowCurse => MintVersion::V1_0_17,
            Game::PlanetRobobot | Game::BoxBoxBoy => MintVersion::V1_1_3,
            Game::BlowoutBlast => MintVersion::V1_1_8,
            Game::TeamKirbyClashDeluxe | Game::ByeByeBoxBoy => MintVersion::V1_1_12,
            Game::BattleRoyale | Game::PartTimeUfo => MintVersion::V2_1_4,
            Game::StarAllies => MintVersion::V2_1_5_1,
        }
    }
}

/// Mint Version list
#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub enum MintVersion {
    V0_2_0,
    V1_0_5,
    V1_0_8,
    V1_0_13,
    V1_0_17,
    V1_1_3,
    V1_1_8,
    V1_1_12,
    V2_1_4,
    V2_1_5_1,
}

impl From<MintVersion> for (u8, u8, u8, u8) {
    fn from(version: MintVersion) -> (u8, u8, u8, u8) {
        match version {
            MintVersion::V0_2_0 => (0, 2, 0, 0),
            MintVersion::V1_0_5 => (1, 0, 5, 0),
            MintVersion::V1_0_8 => (1, 0, 8, 0),
            MintVersion::V1_0_13 => (1, 0, 13, 0),
            MintVersion::V1_0_17 => (1, 0, 17, 0),
            MintVersion::V1_1_3 => (1, 1, 3, 0),
            MintVersion::V1_1_8 => (1, 1, 8, 0),
            MintVersion::V1_1_12 => (1, 1, 12, 0),
            MintVersion::V2_1_4 => (2, 1, 4, 0),
            MintVersion::V2_1_5_1 => (2, 1, 5, 1),
        }
    }
}

impl MintVersion {
    /// Returns the highest supported XBIN version by this Mint Version
    pub fn highest_supported_xbin(self) -> XBinVersion {
        match self {
            MintVersion::V2_1_4 | MintVersion::V2_1_5_1 => XBinVersion::V4(0),
            _ => XBinVersion::V2,
        }
    }
}

/// XBIN verrsion list
#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub enum XBinVersion {
    V2,
    V4(u32),
}

impl From<XBinVersion> for (u8, u8) {
    fn from(version: XBinVersion) -> (u8, u8) {
        match version {
            XBinVersion::V2 => (2, 0),
            XBinVersion::V4(_) => (4, 0),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn system_endian() {
        assert_eq!(Endian::from(System::Wii), Endian::Big);
        assert_eq!(Endian::from(System::N3DS), Endian::Little);
        let system = System::Wii;
        let system2 = system;
        assert_eq!(system, system2);
    }
    #[test]
    fn game_system() {
        assert_eq!(System::from(Game::ReturnToDreamLand), System::Wii);
        assert_eq!(System::from(Game::TripleDeluxe), System::N3DS);
        assert_eq!(System::from(Game::RainbowCurse), System::WiiU);
        assert_eq!(System::from(Game::PartTimeUfo), System::Android);
        assert_eq!(System::from(Game::StarAllies), System::Switch);
        let game = Game::ReturnToDreamLand;
        let game2 = game;
        assert_eq!(game, game2);
    }
    #[test]
    fn game_mint() {
        assert_eq!(
            MintVersion::from(Game::ReturnToDreamLand),
            MintVersion::V0_2_0
        );
        assert_eq!(MintVersion::from(Game::TripleDeluxe), MintVersion::V1_0_5);
        assert_eq!(MintVersion::from(Game::FightersDeluxe), MintVersion::V1_0_8);
        assert_eq!(MintVersion::from(Game::BoxBoy), MintVersion::V1_0_13);
        assert_eq!(MintVersion::from(Game::RainbowCurse), MintVersion::V1_0_17);
        assert_eq!(MintVersion::from(Game::PlanetRobobot), MintVersion::V1_1_3);
        assert_eq!(MintVersion::from(Game::BlowoutBlast), MintVersion::V1_1_8);
        assert_eq!(
            MintVersion::from(Game::TeamKirbyClashDeluxe),
            MintVersion::V1_1_12
        );
        assert_eq!(MintVersion::from(Game::BattleRoyale), MintVersion::V2_1_4);
        assert_eq!(MintVersion::from(Game::StarAllies), MintVersion::V2_1_5_1);
    }
    #[test]
    fn mint_ver_no() {
        assert_eq!(
            Into::<(u8, u8, u8, u8)>::into(MintVersion::V0_2_0),
            (0, 2, 0, 0)
        );
        assert_eq!(
            Into::<(u8, u8, u8, u8)>::into(MintVersion::V1_0_5),
            (1, 0, 5, 0)
        );
        assert_eq!(
            Into::<(u8, u8, u8, u8)>::into(MintVersion::V1_0_8),
            (1, 0, 8, 0)
        );
        assert_eq!(
            Into::<(u8, u8, u8, u8)>::into(MintVersion::V1_0_13),
            (1, 0, 13, 0)
        );
        assert_eq!(
            Into::<(u8, u8, u8, u8)>::into(MintVersion::V1_0_17),
            (1, 0, 17, 0)
        );
        assert_eq!(
            Into::<(u8, u8, u8, u8)>::into(MintVersion::V1_1_3),
            (1, 1, 3, 0)
        );
        assert_eq!(
            Into::<(u8, u8, u8, u8)>::into(MintVersion::V1_1_8),
            (1, 1, 8, 0)
        );
        assert_eq!(
            Into::<(u8, u8, u8, u8)>::into(MintVersion::V1_1_12),
            (1, 1, 12, 0)
        );
        assert_eq!(
            Into::<(u8, u8, u8, u8)>::into(MintVersion::V2_1_4),
            (2, 1, 4, 0)
        );
        assert_eq!(
            Into::<(u8, u8, u8, u8)>::into(MintVersion::V2_1_5_1),
            (2, 1, 5, 1)
        );
    }
    #[test]
    fn mint_xbin() {
        assert_eq!(
            MintVersion::V0_2_0.highest_supported_xbin(),
            XBinVersion::V2
        );
        assert_eq!(
            MintVersion::V2_1_4.highest_supported_xbin(),
            XBinVersion::V4(0)
        );
    }
}
