rm -rf target/debug/mint-* target/debug/ar-* target/debug/nm-* target/debug/objdump-* target/debug/rtdl_assembler-*
RUSTFLAGS="-Ccodegen-units=1 -Clink-dead-code" cargo test --all --no-run
rm -rf target/cov
for f in target/debug/mint-* target/debug/ar-* target/debug/nm-* target/debug/objdump-* target/debug/rtdl_assembler-*; do kcov --verify --include-pattern=$PWD --exclude-pattern=$PWD/cargo target/cov $f || true; done
cat target/cov/kcov-merged/coverage.json | grep '  "percent_covered"'
