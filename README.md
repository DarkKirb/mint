# mint [![pipeline status](https://gitlab.com/DarkKirb/mint/badges/master/pipeline.svg)](https://gitlab.com/DarkKirb/mint/commits/master) [![coverage report](https://gitlab.com/DarkKirb/mint/badges/master/coverage.svg)](https://gitlab.com/DarkKirb/mint/commits/master)

Library and unix-like tools for working with MINT files

## Getting started

TODO

## Built with

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning.

## Authors

- Charlotte - *initial work*

## License

This project is licensed under the 2-Clause BSD License - see the [LICENSE](LICENSE) file for details.

## Acknowledgements

- Reserved - [@reveserved\_kirby](https://twitter.com/reserved_kirby) - Helped a bunch early in reverse-engineering of the Hel-Engine Kirby games
- Firubii - [Firubii](https://github.com/Firubii) - The go-to person for modding Star Allies. Also her code helped me recover what I've forgotten from my recent Hiatus
