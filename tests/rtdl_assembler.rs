use mint::rtdl::assembler::{ast::*, lex};

#[test]
fn ast_test_1() {
    let input = r#"
.section .sdata
hello_world: .pstr "Hello, World!!"
unicode: .cstr "こんにちは、世界"
.section .text
.object User.Tsuruoka.MintTest {
    .member intVal: int;
    static __Init()const{
        ld r0, #0x2d
        ld ["User.Tsuruoka.MintTest.intVal"], r0
        ret
    }
    func1()const{
        ret
    }
    func2()const{
        ld fr0, r0
        call "User.Tsuruoka.MintTest.func1()const"
        ret
    }
    static Hello()const{
        ld r0, hello_world
        ld fr0, r0
        call "Mint.Debug.puts(string)"
        ld r0, unicode
        ld fr0, r0
        call "App.Alert.Print(string)"
        ret
    }
    StaticReturnTest()const->int {
        ld r0, ["User.Tsuruoka.MintTest.intVal"]
        ret
    }
}
"#;
    assert_eq!(
        lex::AssemblyFileParser::new().parse(input).unwrap(),
        vec![
            Stmt::SDataSection(vec![
                SDataExpr::Label("hello_world"),
                SDataExpr::PascalString("Hello, World!!"),
                SDataExpr::Label("unicode"),
                SDataExpr::CString("こんにちは、世界")
            ]),
            Stmt::TextSection(vec![TextExpr::Object(
                "User.Tsuruoka.MintTest".to_string(),
                vec![
                    ObjExpr::Member("int".to_string(), "intVal"),
                    ObjExpr::Function(
                        true,
                        "__Init",
                        vec![],
                        "void".to_string(),
                        true,
                        vec![
                            FunctionExpr::Load(LoadExpr::LoadConstantInt(
                                Register::Integer(0),
                                0x2d
                            )),
                            FunctionExpr::Load(LoadExpr::StoreStatic(
                                "User.Tsuruoka.MintTest.intVal",
                                Register::Integer(0)
                            )),
                            FunctionExpr::Return
                        ]
                    ),
                    ObjExpr::Function(
                        false,
                        "func1",
                        vec![],
                        "void".to_string(),
                        true,
                        vec![FunctionExpr::Return]
                    ),
                    ObjExpr::Function(
                        false,
                        "func2",
                        vec![],
                        "void".to_string(),
                        true,
                        vec![
                            FunctionExpr::Load(LoadExpr::LoadFR(
                                Register::FunctionRegister(0),
                                Register::Integer(0)
                            )),
                            FunctionExpr::Call("User.Tsuruoka.MintTest.func1()const"),
                            FunctionExpr::Return
                        ]
                    ),
                    ObjExpr::Function(
                        true,
                        "Hello",
                        vec![],
                        "void".to_string(),
                        true,
                        vec![
                            FunctionExpr::Load(LoadExpr::LoadConstantAddr(
                                Register::Integer(0),
                                "hello_world"
                            ),),
                            FunctionExpr::Load(LoadExpr::LoadFR(
                                Register::FunctionRegister(0),
                                Register::Integer(0)
                            )),
                            FunctionExpr::Call("Mint.Debug.puts(string)"),
                            FunctionExpr::Load(LoadExpr::LoadConstantAddr(
                                Register::Integer(0),
                                "unicode"
                            ),),
                            FunctionExpr::Load(LoadExpr::LoadFR(
                                Register::FunctionRegister(0),
                                Register::Integer(0)
                            )),
                            FunctionExpr::Call("App.Alert.Print(string)"),
                            FunctionExpr::Return
                        ]
                    ),
                    ObjExpr::Function(
                        false,
                        "StaticReturnTest",
                        vec![],
                        "int".to_string(),
                        true,
                        vec![
                            FunctionExpr::Load(LoadExpr::LoadStatic(
                                Register::Integer(0),
                                "User.Tsuruoka.MintTest.intVal"
                            )),
                            FunctionExpr::Return
                        ]
                    )
                ]
            )])
        ]
    );
}

#[test]
fn cover_all_valid() {
    let input = r#"
.section .sdata
label:
    .uint 1234
    .int -1234
    .float 1e5
    .pstr "pascal string"
    .cstr "c string"
.section .text
.object object {
    .member intval: int;
    fun1() {
        ret
    }
    fun2(type) {
        ret
    }
    fun3(const typea,ref typeb) {
        ret
    }
    static fun4() {
        ret
    }
    static fun5() const{
        ret
    }
    static fun6() const->void{
        ret
    }
    static fun7() -> void {
        ret
    }
    fun8() const{
        ret
    }
    fun9() const->void {
        ret
    }
    fun10() -> void {
        start:
        ld b0, true
        ld b0, false
        ld r0, #0x15
        ld r0, #15
        ld f0, #0.5
        ld r0, [label]
        ld b0, [label]
        ld f0, [label]
        ld r0, label
        ld r0, r0
        ld f0, f0
        ld b0, b0
        ld r0, fr0
        ld b0, fr0
        ld f0, fr0
        ld fr0, r0
        ld fr0, b0
        ld fr0, f0
        ld r0, ["static"]
        ld f0, ["static"]
        ld b0, ["static"]
        ld r0, [r0]
        ld f0, [r0]
        ld b0, [r0]
        ld r0, sizeof:["static"]
        ld [r0], r0
        ld [r0], f0
        ld [r0], b0
        ld ["static"], r0
        ld ["static"], f0
        ld ["static"], b0
        ld r0, sizeof:r0
        add r0, r0, r0
        add f0, f0, f0
        add r0, r0, offset:"static"
        add r0, r0, offset:r0
        sub r0, r0, r0
        sub f0, f0, f0
        muls r0, r0, r0
        mul f0, f0, f0
        divs r0, r0, r0
        div f0, f0, f0
        mods r0, r0, r0
        inc r0
        inc f0
        dec r0
        dec f0
        negs r0, r0
        neg f0, f0
        lts b0, r0, r0
        lt b0, f0, f0
        les b0, r0, r0
        le b0, f0, f0
        eq b0, r0, r0
        eq b0, f0, f0
        eq b0, b0, b0
        ne b0, r0, r0
        ne b0, f0, f0
        ne b0, b0, b0
        ltcmp b0, r0
        lecmp b0, r0
        and r0, r0, r0
        and b0, b0, b0
        or r0, r0, r0
        or b0, b0, b0
        xor r0, r0, r0
        xor b0, b0, b0
        not r0, r0
        not b0, b0
        lsl r0, r0, r0
        lsr r0, r0, r0
        jmp start
        jmppos r0, start
        jmppos f0, start
        jmppos b0, start
        jmpneg r0, start
        jmpneg f0, start
        jmpneg b0, start
        ret
        call "function"
        yield r0
        mcopy [r0], [r0], r0
        mzeros [r0], sizeof:"static"
        new r0, "static"
        newz r0, "static"
        delete [r0], "static"
        new[] r0, r0
        delete[] [r0]
    }
}
    "#;
    let a = lex::AssemblyFileParser::new().parse(input).unwrap();
    let b = a.clone();
    assert_eq!(a, b);
}

#[test]
fn test_syntax() {
    assert!(lex::AssemblyFileParser::new().parse("label: ").is_err());
    assert!(lex::AssemblyFileParser::new()
        .parse(".section .sdata .member")
        .is_err());
    assert!(lex::AssemblyFileParser::new()
        .parse(".section .text .pstr \"hi\"")
        .is_err());
    assert!(lex::AssemblyFileParser::new()
        .parse(".section .sdata .cstr \"hi")
        .is_err());
    assert!(lex::AssemblyFileParser::new()
        .parse(".section .text .object Test.Message {}")
        .is_ok());
    assert!(lex::AssemblyFileParser::new()
        .parse(".section .text .object Test {}")
        .is_ok());
    assert!(lex::AssemblyFileParser::new()
        .parse(".section .text .object Test.Message.Test {}")
        .is_ok());
    assert!(lex::AssemblyFileParser::new()
        .parse(".section .text .object Test. {}")
        .is_err());
    assert!(lex::AssemblyFileParser::new()
        .parse(".section .text .object Test {fn(ref x){}}")
        .is_ok());
    assert!(lex::AssemblyFileParser::new()
        .parse(".section .text .object Test {fn(ref x,x,x,x,const ref y){}}")
        .is_ok());
    assert!(lex::AssemblyFileParser::new()
        .parse(".section .text .object Test {fn(x,){}}")
        .is_err());
    assert!(lex::AssemblyFileParser::new().parse("").is_ok());
    assert!(lex::AssemblyFileParser::new()
        .parse(".section .sdata")
        .is_ok());
    assert!(lex::AssemblyFileParser::new()
        .parse(".section .text")
        .is_ok());
}
